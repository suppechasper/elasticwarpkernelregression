A few dimension reduction methods:

* PCA
* MDS
* Isomap with various ways for computing nearest neighbors
* Principal curves and manifolds using the conditional expectation manifolds approach (includes the R package cems)

![kmm_faces_small.png](https://bitbucket.org/repo/oXAbAa/images/331216223-kmm_faces_small.png)