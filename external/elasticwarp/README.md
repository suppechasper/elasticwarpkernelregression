This is the source code for the elastic registration and
deformable image kernel regression used in the paper "Manifold Modeling for
Brain Population Analysis". 


![oasis_manifold_small.png](https://bitbucket.org/repo/kXnXX8/images/459847312-oasis_manifold_small.png)

It also contains the code for reading and writing
the pairwise distance matrices available from http:://pages.uoregon.edu/~sgerber that
were used in the paper.


Compilation is setup through CMake and requires ITK.