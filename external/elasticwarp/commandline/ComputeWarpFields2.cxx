#define PRECISION
typedef float Precision;

#include "Config.h"


#include "ImageIO.h"
#include "IO.h"
#include "LinalgIO.h"

#include <tclap/CmdLine.h>

//shared memory, process stuff
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>   
#include <sys/wait.h>


int main(int argc, char **argv){

  //Command line parsing
  TCLAP::CmdLine cmd("Compute elastic image warps for selected image pairs", ' ', "1");

  TCLAP::ValueArg<std::string> list1Arg("","list1",
      "List of image to compute warps to images in list2 .", 
      true, "", "filename");
  cmd.add(list1Arg);
  
  TCLAP::ValueArg<std::string> list2Arg("","list2",
      "List of image to compute warps to images in list1.", 
      true, "", "filename");
  cmd.add(list2Arg);
  
  TCLAP::ValueArg<int> nprocArg("p","nproc","number of processors to use", true, 1, "int");
  cmd.add(nprocArg);

  TCLAP::ValueArg<std::string> maskArg("m","mask","mask image", false, "",
      "filename");
  cmd.add(maskArg);

  TCLAP::ValueArg<Precision> stepArg("s","scaling",
      "scaling of maximum step size for gradient descent", false, (Precision)0.2, 
      "step size");
  cmd.add(stepArg);

  TCLAP::ValueArg<int> iterArg("i","iterations",
      "maximum number of iterations per scale", false, 200, 
      "int");
  cmd.add(iterArg);

  TCLAP::ValueArg<Precision> alphaArg("","alpha",
      "weight of gradient (alpha) and identity (1-alpha) penalty", false, (Precision) 0.5, 
      "float");
  cmd.add(alphaArg);
  

  TCLAP::ValueArg<Precision> lambdaArg("","lambda",
      "inital weight of image differnce term", false, (Precision) 1, 
      "float");
  cmd.add(lambdaArg);

  TCLAP::ValueArg<Precision> lambdaIncArg("","lambdaInc",
      "increase in lambda when iamges can not be registered within epsilon for inital given lambda ", 
      false, (Precision) 0.1, 
      "float");
  cmd.add(lambdaIncArg);
  
  TCLAP::ValueArg<Precision> lambdaIncTArg("","lambdaIncThreshold",
      "Threshold in intensity difference between two consecutive updates to update lambda", 
      false, (Precision) 0.0001, 
      "float");
  cmd.add(lambdaIncTArg);

  TCLAP::ValueArg<Precision> epsArg("","epsilon",
      "Maximal root mean squared intensity difference to register images to", false, (Precision) 0.01, 
      "tolerance");
  cmd.add(epsArg);

  TCLAP::ValueArg<Precision> sigmaArg("","sigma",
      "Multiscale sigma of coarsest scale. Sigma is halfed at each subsequent scale"
      , false, (Precision) 8, "float");
  cmd.add(sigmaArg);

  TCLAP::ValueArg<int> nscaleArg("","nscales",
      "Number of scales. 0 corresponds to no scales", false, 5, 
      "int");
  cmd.add(nscaleArg);  
  
  TCLAP::SwitchArg mresArg("", "useMultires", 
      "do multiresultion instead of  multiscale"); 
  cmd.add(mresArg);
 
  try{
	  cmd.parse( argc, argv );
	} 
  catch (TCLAP::ArgException &e){ 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return -1;
  }

   


  std::vector<std::string> imagesList1 =
    IO<Precision>::readStringList(list1Arg.getValue());
  int N1 = imagesList1.size();
  
  std::vector< std::string > images1(N1);
  std::vector<std::string>::iterator fileIt = imagesList1.begin();
  for(int i=0; i < N1; ++i, ++fileIt){
    images1[i] = *fileIt;
  }

  std::vector<std::string> imagesList2 =
    IO<Precision>::readStringList(list2Arg.getValue());
  int N2 = imagesList2.size();
  
  std::vector< std::string > images2( N2 );
  fileIt = imagesList2.begin();
  for(int i=0; i < N2; ++i, ++fileIt){
    images2[i] = *fileIt;
  }


  int nProcInUse = 0;
  int nProcessors = nprocArg.getValue();
  for(int i=0; i < N1 && i < N2 ; i++){
        pid_t pid;
        if(nProcInUse < nProcessors){
          pid = fork();
          nProcInUse++;
        }
        else{
          int status;
          wait(&status);
          pid = fork();
        }
        //Child computes one distance
        if(pid == 0){
          std::stringstream ss;
          ss << "nice ElasticRegistration_" << DIMENSION << "D "; 
          ss << "-f " << images1[i] << " ";
          ss << "-m " << images2[i] << " ";
          ss << "--mask "  << maskArg.getValue() << " ";
          ss << "-d warp_" << i <<".nrrd ";
          //ss << "--info warp_" << i << ".info ";
          ss << "-s " << stepArg.getValue() << " ";
          ss << "-i " << iterArg.getValue() << " ";
          ss << "--alpha " << alphaArg.getValue() << " ";
          ss << "--epsilon " << epsArg.getValue() << " "; 
          ss << "--lambda " << lambdaArg.getValue() << " ";
          ss << "--lambdaInc " << lambdaIncArg.getValue() << " ";
          ss << "--lambdaIncThreshold " << lambdaIncTArg.getValue() << " ";
          ss << "--sigma " << sigmaArg.getValue() << " ";
          ss << "--nscales " << nscaleArg.getValue() << " ";
          if(mresArg.getValue()){
          ss << "--useMultires ";
          }
          std::cout << ss.str() << std::endl;
          std::system(ss.str().c_str());
          return 0;
          
        }
  }

  while(nProcInUse > 0){
    int status;
    wait(&status);
    nProcInUse--;
  }

  return 0;

}
