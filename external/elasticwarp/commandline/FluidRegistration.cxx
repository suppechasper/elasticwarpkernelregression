#include "MyConfig.h"

#include "SimpleFlow.h"
#include "ImageIO.h"


typedef SimpleFlow<Image, DIMENSION> Warp; 

int main(int argc, char **argv){

 if(argc < 6){
   std::cout << "Usage:" << std::endl;
   std::cout << argv[0] << " fixed moving mask warpedFile vectorFieldFile";
   std::cout << " [maxMotion = 0.8] [maxIteration = 200] [diffusionWeight = 0.05]";
   std::cout << " [diffusionWeightDecrease = 0.001]  [diffusionWeightTolerance = 0.01]";
   std::cout << " [averageIntensityTol = 0.05]";
   std::cout << std::endl;
   return 0;
 } 
 
 int argIndex = 1;
 char *fixedFile = argv[argIndex++];
 char *movingFile = argv[argIndex++];
 char *maskFile = argv[argIndex++];
 char *warpedFile = argv[argIndex++];
 char *vectorFieldFile = argv[argIndex++];

 Precision maxMotion = 0.8;
 if(argIndex < argc){
   maxMotion = atof(argv[argIndex++]); 
 }
 int maxIterations = 200;
 if(argIndex < argc){
   maxIterations = atoi(argv[argIndex++]); 
 }
 Precision diffusionWeight = 0.05;
 if(argIndex < argc){
   diffusionWeight = atof(argv[argIndex++]); 
 }
 Precision diffusionWeightDecrease = 0.001;
 if(argIndex < argc){
   diffusionWeightDecrease = atof(argv[argIndex++]); 
 }
 Precision diffusionWeightTolerance = 0.01;
 if(argIndex < argc){
   diffusionWeightTolerance = atof(argv[argIndex++]); 
 }
 Precision averageIntensityTol = 0.05;
 if(argIndex < argc){
  averageIntensityTol = atof(argv[argIndex++]); 
 }


 //Input Images
 typedef Image::Pointer ImagePointer;
 ImagePointer fixed = ImageIO<Image>::readImage(fixedFile);
 ImagePointer moving = ImageIO<Image>::readImage(movingFile);
 ImagePointer mask = ImageIO<Image>::readImage(maskFile);


 //Setup warp
 Warp warp;
 warp.setDiffusionWeight(diffusionWeight);
 warp.setDiffusionWeightDecrease(diffusionWeightDecrease);
 warp.setDiffusionWeightDecreaseTolerance(diffusionWeightTolerance);
 warp.setMaximumIterations(maxIterations);
 warp.setMaximumMotion(maxMotion);
 warp.setDifferenceTolerance(averageIntensityTol);


 //Do warp
 Warp::VImage *vectorfield = warp.warp(moving, fixed, mask);
 ImagePointer warped = Warp::ImageTransform::Transform(moving, vectorfield);
  

 //Output images
 ImageIO<Image>::saveImage(warped, warpedFile);
 ImageIO<Warp::VImage::ITKVectorImage>::saveImage(vectorfield->toITK(), vectorFieldFile);

}

