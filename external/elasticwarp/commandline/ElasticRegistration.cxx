#define PRECISION
typedef float Precision;

#define VERBOSE
#define USE_ORIENTED

#include "MyConfig.h"

#include "ElasticWarp.h"
#include "ImageIO.h"

#include "itkCastImageFilter.h"
#include "itkImageRegionIterator.h"

#include <tclap/CmdLine.h>

typedef SimpleWarp<Image> Warp; 

typedef Image::Pointer ImagePointer;

int main(int argc, char **argv){

  //Command line parsing
  TCLAP::CmdLine cmd("Elastic image registration", ' ', "1");

  TCLAP::ValueArg<std::string> fixedArg("f","fixed","fixed image", true, "",
      "filename");
  cmd.add(fixedArg);

  TCLAP::ValueArg<std::string> movingArg("m","moving","moving image", true, "",
      "filename");
  cmd.add(movingArg);
  
  TCLAP::ValueArg<std::string> movingAddArg("","movingAdd", "moving additional image", false, "",
      "filename");
  cmd.add(movingAddArg);

  TCLAP::ValueArg<std::string> maskArg("k","mask","mask image", false, "",
      "filename");
  cmd.add(maskArg);
  
  TCLAP::ValueArg<std::string> warpedArg("w","warped","warped output image", false, "",
      "filename");
  cmd.add(warpedArg);
  
  TCLAP::ValueArg<std::string> deformArg("d","deformation",
      "deformation vector field output image", false, "", 
      "filename");
  cmd.add(deformArg);

  TCLAP::ValueArg<std::string> infoArg("","info",
      "information file", false, "", 
      "filename");
  cmd.add(infoArg);
  
  TCLAP::ValueArg<Precision> stepArg("s","scaling",
      "scaling of maximum step size for gradient descent", false, (Precision)0.2, 
      "step size");
  cmd.add(stepArg);

  TCLAP::ValueArg<int> iterArg("i","iterations",
      "maximum number of iterations per scale", false, 200, 
      "int");
  cmd.add(iterArg);

  TCLAP::ValueArg<Precision> alphaArg("","alpha",
      "weight of gradient (alpha) and identity (1-alpha) penalty", false, (Precision) 0.5, 
      "float");
  cmd.add(alphaArg);
  

  TCLAP::ValueArg<Precision> lambdaArg("","lambda",
      "inital weight of image differnce term", false, (Precision) 1, 
      "float");
  cmd.add(lambdaArg);

  TCLAP::ValueArg<Precision> lambdaIncArg("","lambdaInc",
      "increase in lambda when images can not be registered within epsilon for inital given lambda ", 
      false, (Precision) 0.1, 
      "float");
  cmd.add(lambdaIncArg);
  
  TCLAP::ValueArg<Precision> lambdaIncTArg("","lambdaIncThreshold",
      "Threshold in intnesity difference between two consectuive updates to update lambda", 
      false, (Precision) 0.0001, 
      "float");
  cmd.add(lambdaIncTArg);

  TCLAP::ValueArg<Precision> epsArg("","epsilon",
      "Maximal root mean squared intensity difference to register images to", false, (Precision) 0.01, 
      "tolerance");
  cmd.add(epsArg);

  TCLAP::ValueArg<Precision> sigmaArg("","sigma",
      "Multiscale sigma of coarsest scale. Sigma is halfed at each subsequent scale"
      , false, (Precision) 8, "float");
  cmd.add(sigmaArg);

  TCLAP::ValueArg<int> nscaleArg("","nscales",
      "Number of scales. 0 corresponds to no scales", false, 5, 
      "int");
  cmd.add(nscaleArg);

  TCLAP::SwitchArg mresArg("", "useMultires", 
      "do multiresultion instead of  multiscale"); 
  cmd.add(mresArg);


  try{
	  cmd.parse( argc, argv );
	} 
  catch (TCLAP::ArgException &e){ 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return -1;
  }

 itk::MultiThreader::SetGlobalMaximumNumberOfThreads(1); 
 //Input Images
 ImagePointer fixed = ImageIO<Image>::readImage(fixedArg.getValue());
 ImagePointer moving = ImageIO<Image>::readImage(movingArg.getValue());
 ImagePointer mask;
 if(maskArg.getValue().size() == 0){
   mask = ImageIO<Image>::copyImage(fixed);
   //set mask to all ones = no masking
   itk::ImageRegionIterator<Image> it(mask, mask->GetLargestPossibleRegion());
   for(; !it.IsAtEnd(); ++it){
    it.Set(1);
   }
 }
 else{
  mask = ImageIO<Image>::readImage(maskArg.getValue());
 }

 //Setup warp
 Warp warp;
 warp.setAlpha(alphaArg.getValue());
 warp.setMaximumIterations(iterArg.getValue());
 warp.setMaximumMotion(stepArg.getValue());
 warp.setEpsilon(epsArg.getValue());
 warp.setLambda(lambdaArg.getValue());
 warp.setLambdaIncrease(lambdaIncArg.getValue());
 warp.setLambdaIncreaseThreshold(lambdaIncTArg.getValue());

 //Do warp
 Warp::VImage *vectorfield = NULL;
 
 if(!mresArg.getValue()){
   vectorfield = warp.warpMultiscale(moving, fixed, mask,
     sigmaArg.getValue(), nscaleArg.getValue());
 }
 else{
   std::cout << "Doing multires warp" << std::endl;
   vectorfield = warp.warpMultiresolution(moving, fixed, mask,
      nscaleArg.getValue(), sigmaArg.getValue());
 }
 
 if(warpedArg.getValue().length() != 0){
  ImagePointer warped = Warp::ImageTransform::Transform(moving, vectorfield);
  ImageIO<Image>::saveImage(warped, warpedArg.getValue());
 }
 std::cout << deformArg.getValue() << std::endl;
 if(deformArg.getValue().length() != 0){
  ImageIO<Warp::VImage::ITKVectorImage>::saveImage(vectorfield->toITK(),
      deformArg.getValue());
 }
 
 if(movingAddArg.getValue().length() != 0){
  ImagePointer movingAdd = ImageIO<Image>::readImage(movingAddArg.getValue());
  ImagePointer warped = Warp::ImageTransform::Transform(movingAdd, vectorfield);
  ImageIO<Image>::saveImage(warped, "movingAdd.mhd");
 }

 if(infoArg.getValue().length() != 0){
   std::ofstream info;
   info.open(infoArg.getValue().c_str());
   info << "Lambda_end: " << warp.getLambda() << std::endl;
   info << "RMSE: " << warp.getRMSE() << std::endl;
   info.close(); 
 }
 delete vectorfield;

 return 0;
}

