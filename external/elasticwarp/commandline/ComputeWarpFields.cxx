#define PRECISION
typedef float Precision;

#include "MyConfig.h"


#include "ImageIO.h"
#include "IO.h"
#include "LinalgIO.h"
#include "Linalg.h"

#include <tclap/CmdLine.h>

//shared memory, process stuff
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>   
#include <sys/wait.h>


int main(int argc, char **argv){

  using namespace FortranLinalg;

  //Command line parsing
  TCLAP::CmdLine cmd("Compute elastic image warps for selected image pairs", ' ', "1");

  TCLAP::ValueArg<std::string> listArg("l","list",
      "List of image to compute warps for.", 
      true, "", "filename");
  cmd.add(listArg);
  
  TCLAP::ValueArg<std::string> indexArg("c","compute",
      "Matrix indicating which paris to compute warps for. One in entry (i,j) indicates warp will not be computed otherwise it will be computed", 
      false, "", "matrix hdr file");
  cmd.add(indexArg);


  TCLAP::ValueArg<int> nprocArg("p","nproc","number of processors to use", true, 1, "int");
  cmd.add(nprocArg);

  TCLAP::ValueArg<std::string> maskArg("m","mask","mask image", false, "",
      "filename");
  cmd.add(maskArg);

  TCLAP::ValueArg<Precision> stepArg("s","scaling",
      "scaling of maximum step size for gradient descent", false, (Precision)0.2, 
      "step size");
  cmd.add(stepArg);

  TCLAP::ValueArg<int> iterArg("i","iterations",
      "maximum number of iterations per scale", false, 200, 
      "int");
  cmd.add(iterArg);

  TCLAP::ValueArg<Precision> alphaArg("","alpha",
      "weight of gradient (alpha) and identity (1-alpha) penalty", false, (Precision) 0.5, 
      "float");
  cmd.add(alphaArg);
  

  TCLAP::ValueArg<Precision> lambdaArg("","lambda",
      "inital weight of image differnce term", false, (Precision) 1, 
      "float");
  cmd.add(lambdaArg);

  TCLAP::ValueArg<Precision> lambdaIncArg("","lambdaInc",
      "increase in lambda when iamges can not be registered within epsilon for inital given lambda ", 
      false, (Precision) 0.1, 
      "float");
  cmd.add(lambdaIncArg);
  
  TCLAP::ValueArg<Precision> lambdaIncTArg("","lambdaIncThreshold",
      "Threshold in intensity difference between two consecutive updates to update lambda", 
      false, (Precision) 0.0001, 
      "float");
  cmd.add(lambdaIncTArg);

  TCLAP::ValueArg<Precision> epsArg("","epsilon",
      "Maximal root mean squared intensity difference to register images to", false, (Precision) 0.01, 
      "tolerance");
  cmd.add(epsArg);

  TCLAP::ValueArg<Precision> sigmaArg("","sigma",
      "Multiscale sigma of coarsest scale. Sigma is halfed at each subsequent scale"
      , false, (Precision) 8, "float");
  cmd.add(sigmaArg);

  TCLAP::ValueArg<int> nscaleArg("","nscales",
      "Number of scales. 0 corresponds to no scales", false, 5, 
      "int");
  cmd.add(nscaleArg);  
  
  TCLAP::SwitchArg mresArg("", "useMultires", 
      "do multiresultion instead of  multiscale"); 
  cmd.add(mresArg);
 
  TCLAP::SwitchArg dplaceArg("", "dplace", 
      ""); 
  cmd.add(dplaceArg);
  
  TCLAP::SwitchArg niceArg("", "nice", 
      ""); 
  cmd.add(niceArg);

  try{
	  cmd.parse( argc, argv );
	} 
  catch (TCLAP::ArgException &e){ 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return -1;
  }

   

  try{

  std::vector<std::string> imagesList =
    IO<Precision>::readStringList(listArg.getValue());
  int N = imagesList.size();
  
  std::vector< std::string > images(N);
  std::vector<std::string>::iterator fileIt = imagesList.begin();
  for(int i=0; i < N; ++i, ++fileIt){
    images[i] = *fileIt;
  }
  
  std::string index = indexArg.getValue();
  DenseMatrix<int> indexMatrix;
  if(index.length() == 0){
    indexMatrix = DenseMatrix<int>(N, N);
    Linalg<int>::Set(indexMatrix, 0);
  }
  else{
    indexMatrix = LinalgIO<int>::readMatrix(indexArg.getValue());
  }

  std::string maskImage = maskArg.getValue();

  int nProcInUse = 0;
  int nProcessors = nprocArg.getValue();
  for(int i=0; i< N; i++){
    for(int j=0; j < N; j++){
        if(indexMatrix(i, j) == 1) continue;
        std::cout << indexMatrix(i, j) << std::endl;

        pid_t pid;
        if(nProcInUse < nProcessors){
          pid = fork();
          nProcInUse++;
        }
        else{
          int status;
          wait(&status);
          pid = fork();
        }
        //Child computes one distance
        if(pid == 0){
          std::stringstream ss;
          if(dplaceArg.getValue()){
            ss << "dplace ";
          }
          if(niceArg.getValue()){
            ss << "nice ";
          }        
          ss << "ElasticRegistration_" << DIMENSION << "D "; 
          ss << "-f " << images[i] << " ";
          ss << "-m " << images[j] << " ";
          if(maskImage.length() > 0){
            ss << "--mask "  << maskArg.getValue() << " ";
          }
          ss << "-d warp_" << i << "_" << j <<".nrrd ";
          //ss << "--info warp_" << i << "_" << j << ".info ";
          ss << "-s " << stepArg.getValue() << " ";
          ss << "-i " << iterArg.getValue() << " ";
          ss << "--alpha " << alphaArg.getValue() << " ";
          ss << "--epsilon " << epsArg.getValue() << " "; 
          ss << "--lambda " << lambdaArg.getValue() << " ";
          ss << "--lambdaInc " << lambdaIncArg.getValue() << " ";
          ss << "--lambdaIncThreshold " << lambdaIncTArg.getValue() << " ";
          ss << "--sigma " << sigmaArg.getValue() << " ";
          ss << "--nscales " << nscaleArg.getValue() << " ";
          if(mresArg.getValue()){
          ss << "--useMultires ";
          }
          std::cout << ss.str() << std::endl;
          std::system(ss.str().c_str());
          return 0;
          
        }
    } 
  }

  while(nProcInUse > 0){
    int status;
    wait(&status);
    nProcInUse--;
  }

  return 0;
  
  }
  catch(char *err){
    std::cerr << err << std::endl;
    return -1;
  }

}
