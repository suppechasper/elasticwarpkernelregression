PROJECT(ELASTIC)

#2D executables
ADD_EXECUTABLE(ElasticRegistration_2D ElasticRegistration.cxx)
TARGET_LINK_LIBRARIES (ElasticRegistration_2D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(ElasticTransform_2D ElasticTransform.cxx)
TARGET_LINK_LIBRARIES (ElasticTransform_2D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(ConcatenatedElasticRegistration_2D ConcatenatedElasticRegistration.cxx)
TARGET_LINK_LIBRARIES (ConcatenatedElasticRegistration_2D ${ITK_LIBRARIES} )
#ADD_EXECUTABLE(ElasticTransformList_2D ElasticTransformList.cxx)
#TARGET_LINK_LIBRARIES (ElasticTransformList_2D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(FluidRegistration_2D FluidRegistration.cxx)
TARGET_LINK_LIBRARIES (FluidRegistration_2D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(ComputeWarpFields_2D ComputeWarpFields.cxx)  
TARGET_LINK_LIBRARIES (ComputeWarpFields_2D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(ComputeWarpFields2_2D ComputeWarpFields2.cxx)  
TARGET_LINK_LIBRARIES (ComputeWarpFields2_2D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(ComputeWarpFields3_2D ComputeWarpFields3.cxx)  
TARGET_LINK_LIBRARIES (ComputeWarpFields3_2D ${ITK_LIBRARIES} )


ADD_EXECUTABLE(WarpDistanceMatrix_2D WarpDistanceMatrix.cxx)
TARGET_LINK_LIBRARIES (WarpDistanceMatrix_2D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(WarpDistanceMatrix3_2D WarpDistanceMatrix3.cxx)
TARGET_LINK_LIBRARIES (WarpDistanceMatrix3_2D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(WarpDistanceVector_2D WarpDistanceVector.cxx)
TARGET_LINK_LIBRARIES (WarpDistanceVector_2D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(WarpResidualVector_2D WarpResidualVector.cxx)
TARGET_LINK_LIBRARIES (WarpResidualVector_2D ${ITK_LIBRARIES} )

SET_TARGET_PROPERTIES(ElasticRegistration_2D ElasticTransform_2D ConcatenatedElasticRegistration_2D  
    FluidRegistration_2D WarpDistanceMatrix_2D WarpDistanceMatrix3_2D 
    WarpDistanceVector_2D ComputeWarpFields_2D ComputeWarpFields2_2D
    ComputeWarpFields3_2D WarpResidualVector_2D PROPERTIES COMPILE_FLAGS
    -DDIMENSION=2)


#3D executables
ADD_EXECUTABLE(ElasticRegistration_3D ElasticRegistration.cxx)
TARGET_LINK_LIBRARIES (ElasticRegistration_3D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(ElasticTransform_3D ElasticTransform.cxx)
TARGET_LINK_LIBRARIES (ElasticTransform_3D ${ITK_LIBRARIES} )

#ADD_EXECUTABLE(ElasticTransformList_3D ElasticTransformList.cxx)
#TARGET_LINK_LIBRARIES (ElasticTransformList_3D ${ITK_LIBRARIES} )


ADD_EXECUTABLE(FluidRegistration_3D FluidRegistration.cxx)
TARGET_LINK_LIBRARIES (FluidRegistration_3D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(ComputeWarpFields_3D ComputeWarpFields.cxx)  
TARGET_LINK_LIBRARIES (ComputeWarpFields_3D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(ComputeWarpFields2_3D ComputeWarpFields2.cxx)  
TARGET_LINK_LIBRARIES (ComputeWarpFields2_3D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(ComputeWarpFields3_3D ComputeWarpFields3.cxx)  
TARGET_LINK_LIBRARIES (ComputeWarpFields3_3D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(ComputeWarpFields4_3D ComputeWarpFields4.cxx)  
TARGET_LINK_LIBRARIES (ComputeWarpFields4_3D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(WarpDistanceMatrix_3D WarpDistanceMatrix.cxx)
TARGET_LINK_LIBRARIES (WarpDistanceMatrix_3D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(WarpDistanceMatrix3_3D WarpDistanceMatrix3.cxx)
TARGET_LINK_LIBRARIES (WarpDistanceMatrix3_3D ${ITK_LIBRARIES} )

#ADD_EXECUTABLE(WarpDistanceMatrix4_3D WarpDistanceMatrix4.cxx)
#TARGET_LINK_LIBRARIES (WarpDistanceMatrix4_3D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(WarpDistanceVector_3D WarpDistanceVector.cxx)
TARGET_LINK_LIBRARIES (WarpDistanceVector_3D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(WarpResidualVector_3D WarpResidualVector.cxx)
TARGET_LINK_LIBRARIES (WarpResidualVector_3D ${ITK_LIBRARIES} )

ADD_EXECUTABLE(WarpResidualMatrix_3D WarpResidualMatrix.cxx)
TARGET_LINK_LIBRARIES (WarpResidualMatrix_3D ${ITK_LIBRARIES} )


SET_TARGET_PROPERTIES(ElasticRegistration_3D  ElasticTransform_3D
    FluidRegistration_3D WarpDistanceMatrix_3D 
    WarpDistanceVector_3D ComputeWarpFields_3D ComputeWarpFields2_3D
    ComputeWarpFields3_3D ComputeWarpFields4_3D WarpResidualVector_3D
    WarpResidualMatrix_3D WarpDistanceMatrix3_3D 
    PROPERTIES COMPILE_FLAGS -DDIMENSION=3)


