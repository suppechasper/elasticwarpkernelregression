#define PRECISION
typedef float Precision;

#include "MyConfig.h"

#include "DenseMatrix.h"
#include "ImageIO.h"
#include "IO.h"
#include "LinalgIO.h"
#include "VectorFieldTransform.h"
#include "VectorImage.h"
#include "MetricImageMetricAdapter.h"
#include "EuclideanMetric.h"

#include "itkImageRegionIterator.h"
#include "itkLinearInterpolateImageFunction.h"

typedef itk::LinearInterpolateImageFunction<Image, Precision> LinearInterpolate;
typedef VectorFieldTransform<Image, Precision, LinearInterpolate> Transform;
typedef Transform::VImage VImage; 
typedef VImage::ITKVectorImage ITKVImage;

typedef Image::Pointer ImagePointer;


int main(int argc, char **argv){
  using namespace FortranLinalg;
  if(argc < 5 ){
    std::cout << "Usage:" << std::endl;
    std::cout << argv[0] << "imlist1 imlist2 mask outfile";
    std::cout << std::endl;
    return 0;
  }

 
   
  int argIndex = 1;
  char *imlist1 = argv[argIndex++];
  char *imlist2 = argv[argIndex++];
  char *maskFile = argv[argIndex++];
  char *outfile = argv[argIndex++];
 
  
  std::vector<std::string> imagesList1 =
    IO<Precision>::readStringList(imlist1);
  int N1 = imagesList1.size();
  
  std::vector< std::string > images1( N1 );
  std::vector<std::string>::iterator fileIt = imagesList1.begin();
  for(int i=0; i < N1; ++i, ++fileIt){
    images1[i] = *fileIt;
  }

  std::vector<std::string> imagesList2 =
    IO<Precision>::readStringList(imlist2);
  int N2 = imagesList2.size();
  
  std::vector<std::string> images2(N2);
  fileIt = imagesList2.begin();
  for(int i=0; i < N2; ++i, ++fileIt){
    images2[i] = *fileIt;
  }
 


  DenseVector<double> distances(std::min(N1, N2));

  EuclideanMetric<Precision> l2metric;
  ImagePointer mask = ImageIO<Image>::readImage(maskFile);
  ImageVectorConverter<Image> converter(mask);

  MetricImageMetricAdapter<Precision, Image> immetric(converter, l2metric);

  //ImagePointer transformed = ImageIO<Image>::createImage(mask);
  ImagePointer transformed = ImageIO<Image>::readImage(maskFile);

  for(int i=0; i< std::min(N1, N2); i++){
      std::stringstream ss;
      ss << "warp_" << i << ".mhd";
      //std::cout << ss.str() << std::endl;
      if( IO<int>::fileExists( ss.str() ) ){
        ITKVImage::Pointer warp = ImageIO<ITKVImage>::readImage( ss.str() );
        ImagePointer fixed = ImageIO<Image>::readImage(images1[i]);
        ImagePointer moving = ImageIO<Image>::readImage(images2[i]);
        Transform::Transform(transformed, moving, warp);
        distances(i) = immetric.distance(transformed, fixed);
      }
      else{
        distances(i) = -1;
      } 
     
  }

  LinalgIO<double>::writeVector(outfile, distances);

  return 0;
}
