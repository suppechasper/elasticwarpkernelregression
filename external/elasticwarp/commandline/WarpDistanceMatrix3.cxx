#define PRECISION
typedef float Precision;

#include "MyConfig.h"

#include "DenseMatrix.h"
#include "ImageIO.h"
#include "IO.h"
#include "LinalgIO.h"
#include "VectorImage.h"
#include "itkImageRegionIterator.h"

typedef VectorImage<Image, DIMENSION> VImage;
typedef VImage::ITKVectorImage ITKVectorImage;
typedef itk::ImageRegionIterator<ITKVectorImage> ITKVectorImageIterator;



int main(int argc, char **argv){
  using namespace FortranLinalg;

  if(argc < 3 ){
    std::cout << "Usage:" << std::endl;
    std::cout << argv[0] << " N  M outfile";
    std::cout << std::endl;
    return 0;
  }

 itk::MultiThreader::SetGlobalMaximumNumberOfThreads(1); 


   
  int argIndex = 1;
  int N = atoi(argv[argIndex++]);
  int M = atoi(argv[argIndex++]);
  char *outfile = argv[argIndex++];

  DenseMatrix<double> jf(N, M);
  DenseMatrix<double> mag(N, M);


  for(int i=0; i< N; i++){
    std::cout << i << std::endl;
    for(int j=0; j < M; j++){
      std::stringstream ss;
      ss << "warp_" << i << "_" << j << ".nrrd";
      //std::cout << ss.str() << std::endl;
      if( IO<int>::fileExists( ss.str() ) ){
        ITKVectorImage::Pointer warp = ImageIO<ITKVectorImage>::readImage( ss.str() );
        VImage v(warp);
        jf(i, j) = v.sumJacobianFrobeniusSquared();
        mag(i, j) = v.sumMagnitudeSquared();

      }
      else{
        jf(i, j) = -1;
        mag(i, j) = -1;
      } 
    } 
  }
  std::cout << std::endl;


  std::stringstream ss1;
  ss1 << outfile <<"_jf.data";
  LinalgIO<double>::writeMatrix(ss1.str(), jf);

  std::stringstream ss2;
  ss2 << outfile <<"_mag.data";
  LinalgIO<double>::writeMatrix(ss2.str(), mag);


  return 0;

}
