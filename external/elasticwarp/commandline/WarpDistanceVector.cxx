#define PRECISION
typedef float Precision;

#include "MyConfig.h"

#include "DenseMatrix.h"
#include "ImageIO.h"
#include "IO.h"
#include "LinalgIO.h"
#include "VectorImage.h"

typedef VectorImage<Image, DIMENSION> VImage;
typedef VImage::ITKVectorImage ITKVectorImage;



int main(int argc, char **argv){
  using namespace FortranLinalg;
  if(argc < 3 ){
    std::cout << "Usage:" << std::endl;
    std::cout << argv[0] << " N outfile";
    std::cout << std::endl;
    return 0;
  }

 
   
  int argIndex = 1;
  int N = atoi(argv[argIndex++]);
  char *outfile = argv[argIndex++];


  DenseVector<double> jf(N);
  DenseVector<double> mag(N);


  for(int i=0; i< N; i++){
      std::stringstream ss;
      ss << "warp_" << i << ".mhd";
      //std::cout << ss.str() << std::endl;
      if( IO<int>::fileExists( ss.str() ) ){
        ITKVectorImage::Pointer warp = ImageIO<ITKVectorImage>::readImage( ss.str() );
        VImage v(warp);
        jf(i) = v.sumJacobianFrobeniusSquared();
        mag(i) = v.sumMagnitudeSquared();

      }
      else{
        jf(i) = -1;
        mag(i) = -1;
      } 
    
  }


  std::stringstream ss1;
  ss1 << outfile <<"_jf.data";
  LinalgIO<double>::writeVector(ss1.str(), jf);

  std::stringstream ss2;
  ss2 << outfile <<"_mag.data";
  LinalgIO<double>::writeVector(ss2.str(), mag);


  return 0;

}
