#define PRECISION
typedef float Precision;

#include "MyConfig.h"

#include "DenseMatrix.h"
#include "ImageIO.h"
#include "IO.h"
#include "LinalgIO.h"
#include "VectorImage.h"
#include "VectorFieldTransform.h"
#include "VectorImage.h"
#include "MetricImageMetricAdapter.h"
#include "EuclideanMetric.h"

#include "itkImageRegionIterator.h"
#include "itkLinearInterpolateImageFunction.h"



typedef itk::LinearInterpolateImageFunction<Image, Precision> LinearInterpolate;
typedef VectorFieldTransform<Image, Precision, LinearInterpolate> Transform;
typedef Transform::VImage VImage; 
typedef VImage::ITKVectorImage ITKVImage;
typedef Image::Pointer ImagePointer;



int main(int argc, char **argv){
  using namespace FortranLinalg;

  if(argc < 4 ){
    std::cout << "Usage:" << std::endl;
    std::cout << argv[0] << " imlist mask outfile";
    std::cout << std::endl;
    return 0;
  }

 
   
  int argIndex = 1;
  char *imList = argv[argIndex++];
  char *maskFile = argv[argIndex++];
  char *outfile = argv[argIndex++];

  std::vector<std::string> imagesList =
    IO<Precision>::readStringList(imList);
  int N = imagesList.size();
  
  std::vector<std::string> images( N );
  std::vector<std::string>::iterator fileIt = imagesList.begin();
  for(int i=0; i < N; ++i, ++fileIt){
    images[i] = *fileIt;
  }

  DenseMatrix<double> distances(N, N);

  EuclideanMetric<Precision> l2metric;
  ImagePointer mask = ImageIO<Image>::readImage(maskFile);
  ImageVectorConverter<Image> converter(mask);

  MetricImageMetricAdapter<Precision, Image> immetric(converter, l2metric);

  ImagePointer transformed = ImageIO<Image>::readImage(maskFile);
  //ImagePointer transformed = ImageIO<Image>::createImage(mask);
  
  for(int i=0; i< N; i++){
    for(int j=0; j < N; j++){
      std::stringstream ss;
      ss << "warp_" << i << "_" << j << ".mhd";
      //std::cout << ss.str() << std::endl;
      if( IO<int>::fileExists( ss.str() ) ){
        ITKVImage::Pointer warp = ImageIO<ITKVImage>::readImage( ss.str() );
        ImagePointer fixed = ImageIO<Image>::readImage(images[i]);
        ImagePointer moving = ImageIO<Image>::readImage(images[j]);
        Transform::Transform(transformed, moving, warp);
        distances(i, j) = immetric.distance(transformed, fixed);
      }
      else{
        distances(i, j) = -1;
      } 
    } 
  }


  LinalgIO<double>::writeMatrix(outfile, distances);


  return 0;

}
