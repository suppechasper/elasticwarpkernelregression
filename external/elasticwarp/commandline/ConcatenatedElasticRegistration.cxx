#define PRECISION
typedef float Precision;

#define VERBOSE
#define USE_ORIENTED

#include "MyConfig.h"

#include "ElasticWarp.h"
#include "ImageIO.h"
#include "IO.h"

#include "itkCastImageFilter.h"
#include "itkAddImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkImageDuplicator.h"

#include <tclap/CmdLine.h>

#include <iostream>
#include <iomanip>


typedef SimpleWarp<Image> Warp; 

typedef Image::Pointer ImagePointer;

typedef Warp::ImageTransform ImageTransform; 
typedef ImageTransform::VImage VImage; 
 

typedef itk::AddImageFilter <Image, Image > AddImageFilter;


int main(int argc, char **argv){

  //Command line parsing
  TCLAP::CmdLine cmd("Concatednated elastic image registration", ' ', "1");

  TCLAP::ValueArg<std::string> seqArg("l","list","list of order images - compute registration between each consecutive pair", true, "",  "filename");
  cmd.add(seqArg);


  TCLAP::ValueArg<std::string> maskArg("k","mask","mask image", false, "",
      "filename");
  cmd.add(maskArg);

  TCLAP::ValueArg<std::string> deformArg("d","deformation",
      "deformation vector field output image", false, "", 
      "filename");
  cmd.add(deformArg);

  TCLAP::ValueArg<std::string> infoArg("","info",
      "information file", false, "", 
      "filename");
  cmd.add(infoArg);

  TCLAP::ValueArg<Precision> stepArg("s","scaling",
      "scaling of maximum step size for gradient descent", false, (Precision)0.2, 
      "step size");
  cmd.add(stepArg);

  TCLAP::ValueArg<int> iterArg("i","iterations",
      "maximum number of iterations per scale", false, 200, 
      "int");
  cmd.add(iterArg);

  TCLAP::ValueArg<Precision> alphaArg("","alpha",
      "weight of gradient (alpha) and identity (1-alpha) penalty", false, (Precision) 0.5, 
      "float");
  cmd.add(alphaArg);


  TCLAP::ValueArg<Precision> lambdaArg("","lambda",
      "inital weight of image differnce term", false, (Precision) 1, 
      "float");
  cmd.add(lambdaArg);

  TCLAP::ValueArg<Precision> lambdaIncArg("","lambdaInc",
      "increase in lambda when images can not be registered within epsilon for inital given lambda ", 
      false, (Precision) 0.1, 
      "float");
  cmd.add(lambdaIncArg);

  TCLAP::ValueArg<Precision> lambdaIncTArg("","lambdaIncThreshold",
      "Threshold in intnesity difference between two consectuive updates to update lambda", 
      false, (Precision) 0.0001, 
      "float");
  cmd.add(lambdaIncTArg);

  TCLAP::ValueArg<Precision> epsArg("","epsilon",
      "Maximal root mean squared intensity difference to register images to", false, (Precision) 0.01, 
      "tolerance");
  cmd.add(epsArg);

  TCLAP::ValueArg<Precision> sigmaArg("","sigma",
      "Multiscale sigma of coarsest scale. Sigma is halfed at each subsequent scale"
      , false, (Precision) 8, "float");
  cmd.add(sigmaArg);

  TCLAP::ValueArg<int> nscaleArg("","nscales",
      "Number of scales. 0 corresponds to no scales", false, 0, 
      "int");
  cmd.add(nscaleArg);

  TCLAP::SwitchArg mresArg("", "useMultires", 
      "do multiresultion instead of  multiscale"); 
  cmd.add(mresArg);


  try{
    cmd.parse( argc, argv );
  } 
  catch (TCLAP::ArgException &e){ 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return -1;
  }

  itk::MultiThreader::SetGlobalMaximumNumberOfThreads(1);



  //sequence of images
  std::vector< std::string > seq = IO<Precision>::readStringList( seqArg.getValue() );
  std::vector< std::string >::iterator it = seq.begin();
  ImagePointer moving = ImageIO<Image>::readImage(*it);
  ++it;

  typedef itk::ImageDuplicator< Image > DuplicatorType;
  DuplicatorType::Pointer duplicator = DuplicatorType::New();
  duplicator->SetInputImage(moving);
  duplicator->Update();
  ImagePointer moving0 = duplicator->GetOutput();
        

  //Input Images
  ImagePointer mask;
  if(maskArg.getValue().size() == 0){
    mask = ImageIO<Image>::copyImage(moving);
    //set mask to all ones = no masking
    itk::ImageRegionIterator<Image> it(mask, mask->GetLargestPossibleRegion());
    for(; !it.IsAtEnd(); ++it){
      it.Set(1);
    }
  }
  else{
    mask = ImageIO<Image>::readImage(maskArg.getValue());
  }

  
  VImage *transform = Warp::ImageTransform::InitializeZeroTransform(moving);

  int iter=0;
  for(;it != seq.end(); ++it, ++iter){
    ImagePointer fixed = ImageIO<Image>::readImage(*it);

    //Setup warp
    Warp warp;
    warp.setAlpha(alphaArg.getValue());
    warp.setMaximumIterations(iterArg.getValue());
    warp.setMaximumMotion(stepArg.getValue());
    warp.setEpsilon(epsArg.getValue());
    warp.setLambda(lambdaArg.getValue());
    warp.setLambdaIncrease(lambdaIncArg.getValue());
    warp.setLambdaIncreaseThreshold(lambdaIncTArg.getValue());

    //Do warp
    Warp::VImage *vectorfield = NULL;

    if(!mresArg.getValue()){
      vectorfield = warp.warpMultiscale(moving, fixed, mask,
          sigmaArg.getValue(), nscaleArg.getValue() );
    }
    else{
      std::cout << "Doing multires warp" << std::endl;
      vectorfield = warp.warpMultiresolution(moving, fixed, mask,
          nscaleArg.getValue(), sigmaArg.getValue());
    }

 #ifdef VERBOSE
    std::cout << "Lambda_end: " << warp.getLambda() << std::endl;
    std::cout << "RMSE: " << warp.getRMSE() << std::endl;
#endif
  

  //  delete transform;
  //  transform = vectorfield;

    
    //compose vectorfield with current transform

    VImage *vT = ImageTransform::InitializeZeroTransform(moving);
    ImagePointer *compsT = vT->getComps();
    ImagePointer *compsV = vectorfield->getComps();
    for(int i=0; i < DIMENSION; i++){
      ImageTransform::Transform(compsT[i], compsV[i], transform);
    }
    
    ImagePointer *comps = transform->getComps();
    for(int i=0; i < DIMENSION; i++){
 
      AddImageFilter::Pointer add = AddImageFilter::New();
      add->SetInput1(compsT[i]);
      add->SetInput2(comps[i]);
      add->Update();
      
      transform->setComp(i, add->GetOutput()); 
    }

    delete vT;
    delete vectorfield;

    //moving = fixed;     
    moving = ImageTransform::Transform(moving0, transform);
    //std::stringstream ss;
    //ss << "m" << std::setfill('0') << std::setw(3) << iter << ".nrrd";
    //ImageIO<Image>::saveImage(moving, ss.str());
  }

  ImageIO<Warp::VImage::ITKVectorImage>::saveImage(transform->toITK(),
      deformArg.getValue());
  return 0;
}

