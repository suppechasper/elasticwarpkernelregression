#define PRECISION
typedef float Precision;


#include "Config.h"

#include "ElasticWarp.h"
#include "ImageIO.h"

#include "itkCastImageFilter.h"
#include "itkImageRegionIterator.h"

#include <tclap/CmdLine.h>

typedef SimpleWarp<Image> Warp; 

#define VERBOSE
#define USE_ORIENTED

int main(int argc, char **argv){

  //Command line parsing
  TCLAP::CmdLine cmd("Vectorfield image deformation", ' ', "1");

  TCLAP::ValueArg<std::string> movingArg("m","moving","moving image", true, "",
      "filename");
  cmd.add(movingArg);

  TCLAP::ValueArg<std::string> warpedArg("w","warped","warped output image", false, "",
      "filename");
  cmd.add(warpedArg);
  
  TCLAP::ValueArg<std::string> deformArg("d","deformation",
      "deformation vector field output image", false, "", 
      "filename");
  cmd.add(deformArg);

  




  try{
	  cmd.parse( argc, argv );
	} 
  catch (TCLAP::ArgException &e){ 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return -1;
  }


 //Input Images
 ImagePointer moving = ImageIO<Image>::readImage(movingArg.getValue());

 Warp::VImage::ITKVectorImage::Pointer vectorfield 
   = ImageIO<Warp::VImage::ITKVectorImage>::readImage(deformArg.getValue());
 
 Warp::VImage *vimage = new Warp::VImage(vectorfield);
 ImagePointer warped = Warp::ImageTransform::Transform(moving, vimage);
 ImageIO<Image>::saveImage(warped, warpedArg.getValue());
 
 ImageIO<Warp::VImage::ITKVectorImage>::saveImage(vimage->toITK(),
     "test.mhd");
 

 return 0;
}

