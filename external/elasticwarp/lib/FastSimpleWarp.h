#ifndef FASTSIMPLEWARP_H
#define FASTSIMPLEWARP_H

#include "VectorFieldTransform.h"


#include "itkGradientImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkMinimumMaximumImageFilter.h"
#include "itkDiscreteGaussianImageFilter.h"
#include "itkSmoothingRecursiveGaussianImageFilter.h"
#include "itkLinearInterpolateImageFunction.h"
#include "itkVectorLinearInterpolateImageFunction.h"

template<typename TImage>
class FastSimpleWarp{

  public:


    typedef TImage Image;
    typedef typename Image::Pointer ImagePointer;
    typedef typename Image::PixelType Precision;
    typedef typename Image::IndexType ImageIndex;

    typedef typename itk::LinearInterpolateImageFunction<Image, Precision>
      LinearInterpolate;
    typedef typename LinearInterpolate::Pointer LinearInterpolatePointer;
    typedef typename LinearInterpolate::ContinuousIndexType ImageContinuousIndex;

    typedef VectorFieldTransform<Image, Precision, LinearInterpolate>
      ImageTransform;

    typedef typename ImageTransform::VImage VImage;
    typedef typename VImage::VectorType VectorType;




    FastSimpleWarp(){
      nIterations = 100;
      diffusionWeight = 100;
      maxMotion = 0.8;
      averageDiffTolerance = 0.001;
    };



    //Compute vectorfield v minimizing  
    // Int ( i1(x + v(x)) - i2 ) dx + alpha * Int | grad v(x) |^2 dx  
    VImage *warp(ImagePointer i1, ImagePointer i2, ImagePointer mask){

      /*
      static Precision EPSILON = 1.0e-20;

      //Transformation vector field (v)
      VectorImagePointer transform = ImageTransform::InitializeZeroTransform(i1); 
      VectorImagePointer transformUpdate = ImageTransform::InitializeZeroTransform(i1); 

        

      
      //Previous transform for computing update differences
      VectorImagePointer transformPrevious = ImageTransform::InitializeZeroTransform(i1); 



      //Interpolate function for obtaining i1(x+v(x))
      LinearInterpolatePointer i1Interpolate = LinearInterpolate::New();
      i1Interpolate->SetInputImage(i1);

      //Gradient of target image
      GradientImageFilterPointer grad = GradientImageFilter::New();
      grad->SetInput(i1);
      grad->Update(); 
      GradientImagePointer gi1 = grad->GetOutput();

      //Interpolate function for obtaining  grad(i1)(x+v(x))
      GradientLinearInterpolatePointer gi1Interpolate =
        GradientLinearInterpolate::New();
      gi1Interpolate->SetInputImage(gi1);

      //ImageIO<GradientImage>::saveImage(gi1, "grad.mhd");
      //
      itk::Vector<Precision, ::itk::GetImageDimension<Image>::ImageDimension> tmpVector; 

      //Vector field blurring (smoothing term)
      //GaussianFilterPointer blur = GaussianFilter::New();
      //blur->SetInput(transform);


      //Region information of transformation
      ImageRegion region = i1->GetLargestPossibleRegion();
      ImageSize size = region.GetSize();
      ImageIndex index = region.GetIndex();


      //Iterators for the various images
      VectorImageIterator vIt(transform, region); 
      VectorImageIterator uvIt(transformUpdate, region);
      GradientImageIterator gi1It(gi1, region);
      IndexedImageIterator i1It(i1, region);
      ImageIterator i2It(i2, region);
      ImageIterator maskIt(mask, region);

      
      //number of pixels in mask
      int nValid = 0;
      for(maskIt.GoToBegin(); !maskIt.IsAtEnd(); ++maskIt){
          if(maskIt.Get() != 0){
            nValid++;
          }
      };

      //Iterate until maximum number of iterations or 
      //stopping criteria is achieved        
      for(int i=0; i<nIterations; i++){
        //std::cout << "Iteration: " << i << std::endl; 


        //1. Compute Vector Field update

        Precision maxUpdate = 0;
        for(uvIt.GoToBegin(), i1It.GoToBegin(), i2It.GoToBegin(),
            gi1It.GoToBegin(), vIt.GoToBegin(), maskIt.GoToBegin(); 
            !uvIt.IsAtEnd(); ++i1It, ++i2It, ++gi1It, ++uvIt, ++vIt, 
            ++maskIt){

          if(maskIt.Get() != 0){

            //location cindex = x + v(x)
            ImageIndex x = i1It.GetIndex();
            VectorType v = vIt.Get();
            ImageContinuousIndex cindex;

            for(int i=0; i<ImageIndex::GetIndexDimension(); i++){
              cindex[i] = x[i] + v[i];
              //check range of index
              if(cindex[i] < index[i]){
                cindex[i] = index[i];
              }
              else if(cindex[i] > index[i] + size[i] - 1){
                cindex[i] = index[i] + size[i] - 1;
              }
            };

            //Gradient of i1 at x + v(x) 
            GradientInterpolateOutput g = gi1Interpolate->EvaluateAtContinuousIndex(cindex);

            //Value of i1 at x + v(x)
            Precision i1w = i1Interpolate->EvaluateAtContinuousIndex(cindex);

            tmpVector = g.GetDataPointer();
            tmpVector *= -( i1w -i2It.Get() ) ;
            Precision tmpLength = tmpVector.GetSquaredNorm();
            if(tmpLength > maxUpdate){
              maxUpdate = tmpLength;
            }
            uvIt.Set(  tmpVector  );
          }
        }


        //2. Compute Time Step
        //std::cout <<"MaxUpdate: " << sqrt(maxUpdate) << std::endl;
        //Time step
        Precision dt = maxMotion/( sqrt(maxUpdate) + EPSILON );
        //std::cout <<"dt: " << dt << std::endl;

        //3. Apply update time step dt
        for(uvIt.GoToBegin(), vIt.GoToBegin(), maskIt.GoToBegin(), 
            vIt.GoToBegin(); !uvIt.IsAtEnd(); ++uvIt, ++vIt, ++maskIt){
          if(maskIt.Get()!=0){
            vIt.Set( vIt.Get() + dt * uvIt.Get() );
          }
        }


        //4. Compute image difference
        Precision maxDiff = 0;
        Precision sumDiff = 0; 
        Precision diff;
        for(i1It.GoToBegin(), vIt.GoToBegin(), maskIt.GoToBegin(), 
            i2It.GoToBegin(); !vIt.IsAtEnd(); ++i1It, ++i2It, ++vIt, ++maskIt){

          if(maskIt.Get()!=0){

            //location cindex = x + v(x)
            ImageIndex x = i1It.GetIndex();
            VectorType v = vIt.Get();
            ImageContinuousIndex cindex;

            for(int i=0; i<ImageIndex::GetIndexDimension(); i++){
              cindex[i] = x[i] + v[i];
              //check range of index
              if(cindex[i] < index[i]){
                cindex[i] = index[i];
              }
              else if(cindex[i] > index[i] + size[i] - 1){
                cindex[i] = index[i] + size[i] - 1;
              }
            };

            //Value of i1 at x + v(x)
            Precision i1w = i1Interpolate->EvaluateAtContinuousIndex(cindex);

            diff = (i2It.Get() - i1w);
            diff *= diff;
            sumDiff += diff;
            if(maxDiff < diff){
              maxDiff = diff;
            }
          }
        }

        sumDiff = sumDiff / nValid;
        //std::cout << "iteration: " << i << std::endl;
        //std::cout << "MaxDiff: " << maxDiff << std::endl;
        //std::cout << "AvgDiff: " << sumDiff << std::endl;

        //Check stopping criteria
        if(sumDiff < averageDiffTolerance ){
          break;
        }

        //std::cout << " done" << std::endl;          
      }

      //Smooth resulting transform vectorfield
      GaussianFilterPointer blur = GaussianFilter::New();
      blur->SetInput(transform);
      blur->SetSigma(diffusionWeight);
      blur->Update();


      return blur->GetOutput();
      */
        return NULL:
    };


    void setDiffusionWeight(Precision diffw){
      diffusionWeight = diffw;
    };
    
    Precision getDiffusionWeight(){
      return diffusionWeight;
    };

    void setMaximumMotion(Precision maxm){
      maxMotion = maxm;
    };

    void setMaximumIterations(int maxIter){
      nIterations = maxIter;
    };

    void setAverageDifferenceTolerance(Precision tol){
      averageDiffTolerance = tol;
    };

    VImage *getVectorField(){
      return transform;
    };




  private:

    typedef typename itk::GradientImageFilter<Image, Precision, Precision>
      GradientImageFilter;
    typedef typename GradientImageFilter::Pointer GradientImageFilterPointer;

    typedef typename GradientImageFilter::OutputImageType GradientImage;
    typedef typename GradientImage::Pointer GradientImagePointer;
    typedef typename GradientImage::PixelType GradientType;

    typedef typename itk::ImageRegionIterator<Image> ImageIterator;
    typedef typename itk::ImageRegionIteratorWithIndex<Image> IndexedImageIterator;
    typedef typename itk::ImageRegionIterator<VectorImage> VectorImageIterator;
    typedef typename itk::ImageRegionIterator<GradientImage> GradientImageIterator;

    //typedef typename itk::DiscreteGaussianImageFilter<VectorImage, VectorImage>
    //  GaussianFilter;
    typedef typename itk::SmoothingRecursiveGaussianImageFilter<VectorImage,
            VectorImage> GaussianFilter;
    typedef typename GaussianFilter::Pointer GaussianFilterPointer;

    typedef typename itk::VectorLinearInterpolateImageFunction<GradientImage, Precision>
      GradientLinearInterpolate;
    typedef typename GradientLinearInterpolate::Pointer GradientLinearInterpolatePointer;
    typedef typename GradientLinearInterpolate::OutputType
      GradientInterpolateOutput;

    





    VectorImagePointer transform;
    Precision maxMotion;
    Precision diffusionWeight;
    Precision averageDiffTolerance;
    int nIterations;  


};


#endif
