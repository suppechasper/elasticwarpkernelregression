#ifndef SIMPLEWARP_H
#define SIMPLEWARP_H

#include "VectorFieldTransform.h"


#include "itkGradientImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkMinimumMaximumImageFilter.h"
#include "itkDiscreteGaussianImageFilter.h"
#include "itkSmoothingRecursiveGaussianImageFilter.h"
#include "itkLinearInterpolateImageFunction.h"
#include "itkVectorLinearInterpolateImageFunction.h"

#ifndef VERBOSE
#define VERBOSE
#endif

template<typename TImage>
class SimpleWarp{

  public:


    typedef TImage Image;
    typedef typename Image::Pointer ImagePointer;
    typedef typename Image::PixelType TPrecision;
    typedef typename Image::IndexType ImageIndex;

    typedef typename itk::LinearInterpolateImageFunction<Image, TPrecision>
      LinearInterpolate;
    typedef typename LinearInterpolate::Pointer LinearInterpolatePointer;
    typedef typename LinearInterpolate::ContinuousIndexType ImageContinuousIndex;

    typedef VectorFieldTransform<Image, TPrecision, LinearInterpolate>
      ImageTransform;

    typedef typename ImageTransform::VImage VImage;
    typedef typename VImage::VectorType VectorType;



    SimpleWarp(){
      nIterations = 200;
      diffusionWeight = 0.05;
      maxMotion = 0.2;
      diffTolerance = 0.025;
    };

 
    //Do a multiscale warp starting with a gaussian blur with var = sigma^2
    //and do nsteps each with sigma = sigma/2 of the previous sigma plus a last
    //step with the original images 
    VImage *warpMultiscale(ImagePointer i1, ImagePointer i2, ImagePointer mask,
        TPrecision sigma, int nsteps, VImage *initTransform = NULL){
      
      VImage *transform = NULL;
      VImage *prevTransform = initTransform;
      if(prevTransform != NULL){
        prevTransform = new VImage();
        prevTransform->copy(initTransform);
      }

      //Multiscale steps
      for(int i=0; i<nsteps; i++){
        GaussianFilterPointer smooth1 = GaussianFilter::New();
        smooth1->SetSigma(sigma);
        smooth1->SetInput(i1);
        smooth1->Update();
        GaussianFilterPointer smooth2 = GaussianFilter::New();
        smooth2->SetSigma(sigma);
        smooth2->SetInput(i2);
        smooth2->Update();

        transform = warp(smooth1->GetOutput(), smooth2->GetOutput(), mask,
            prevTransform);
        delete prevTransform;
        prevTransform = transform;

        sigma = sigma/2;
      }

      transform = warp(i1, i2, mask, transform);
      return transform;    
    };


    //Compute vectorfield v minimizing  
    // Int ( i1(x + v(x)) - i2 ) dx + alpha * Int | grad v(x) |^2 dx  
    VImage *warp(ImagePointer i1, ImagePointer i2, ImagePointer mask,
        VImage *initTransform = NULL){

      static TPrecision EPSILON = 1.0e-20;
      const int dimension = ::itk::GetImageDimension<Image>::ImageDimension;

      //Transformation vector field (v)
      VImage *transform;
      if(initTransform == NULL){
        transform = ImageTransform::InitializeZeroTransform(i1);
      }
      else{
        transform = new VImage();
        transform->copy(initTransform);
      }
      VImage *transformUpdate = ImageTransform::InitializeZeroTransform(i1); 
 


      //Interpolate function for obtaining i1(x+v(x))
      LinearInterpolatePointer i1Interpolate = LinearInterpolate::New();
      i1Interpolate->SetInputImage(i1);

      //Gradient of target image
      GradientImageFilterPointer grad = GradientImageFilter::New();
      grad->SetInput(i1);
      grad->Update(); 
      GradientImagePointer gi1 = grad->GetOutput();
      itk::Vector<TPrecision, dimension> tmpVector;

      //Interpolate function for obtaining  grad(i1)(x+v(x))
      GradientLinearInterpolatePointer gi1Interpolate =
        GradientLinearInterpolate::New();
      gi1Interpolate->SetInputImage(gi1);

      //ImageIO<GradientImage>::saveImage(gi1, "grad.mhd");

      //Region information of transformation
      ImageRegion region = i1->GetLargestPossibleRegion();
      ImageSize size = region.GetSize();
      ImageIndex index = region.GetIndex();


      //Iterators for the various images
      VectorType v;
      VectorType uv;
      VectorType vt;
      VectorType vp;


      transform->initIteration(region); 
      transformUpdate->initIteration(region); 

      GradientImageIterator gi1It(gi1, region);
      IndexedImageIterator i1It(i1, region);
      ImageIterator i2It(i2, region);
      ImageIterator maskIt(mask, region);

      
      //number of pixels in mask
      int nValid = 0;
      for(maskIt.GoToBegin(); !maskIt.IsAtEnd(); ++maskIt){
          if(maskIt.Get() != 0){
            nValid++;
          }
      };


      //Iterate until maximum number of iterations or 
      //stopping criteria is achieved   
      int nIter = 0;     
      TPrecision prevSumDiffIntensity = std::numeric_limits<TPrecision>::max();

      for(;nIter<nIterations; nIter++){

        //1. Compute Vector Field update
        TPrecision diffIntensity = 0;
        TPrecision maxUpdate = 0;
        TPrecision sumDiffIntensity = 0;

        transformUpdate->goToBegin();
        transform->goToBegin();
        i1It.GoToBegin();
        i2It.GoToBegin();
        gi1It.GoToBegin(); 
        maskIt.GoToBegin();
        for(; !maskIt.IsAtEnd(); ++i1It, ++i2It, ++gi1It, transform->next(),
            transformUpdate->next(), ++maskIt){

          if(maskIt.Get() != 0){

            //location cindex = x + v(x)
            ImageIndex x = i1It.GetIndex();
            transform->get(v);
            ImageContinuousIndex cindex;

            for(unsigned int i=0; i<ImageIndex::GetIndexDimension(); i++){
              cindex[i] = x[i] + v[i];
              //check range of index
              if(cindex[i] < index[i]){
                cindex[i] = index[i];
              }
              else if(cindex[i] > index[i] + size[i] - 1){
                cindex[i] = index[i] + size[i] - 1;
              }
            };

            //Gradient of i1 at x + v(x) 
            GradientInterpolateOutput g = gi1Interpolate->EvaluateAtContinuousIndex(cindex);

            //Value of i1 at x + v(x)
            TPrecision i1w = i1Interpolate->EvaluateAtContinuousIndex(cindex);

            for(int i=0; i<dimension; i++){
              tmpVector[i] = (TPrecision) g[i];
            }
            diffIntensity = i1w - i2It.Get(); 
            sumDiffIntensity += diffIntensity * diffIntensity;
            tmpVector *= -(diffIntensity);
            TPrecision tmpLength = tmpVector.GetSquaredNorm();
            if(tmpLength > maxUpdate){
              maxUpdate = tmpLength;
            }
            transformUpdate->set(  tmpVector  );
          }
        }

        sumDiffIntensity = sqrt(sumDiffIntensity/nValid);
        diffEnd = sumDiffIntensity;

        //Check if we are within tolerance
        if(fabs(sumDiffIntensity  - prevSumDiffIntensity)< diffTolerance){
          break;
        }

        if(sumDiffIntensity > prevSumDiffIntensity){
          std::cout << "Cannot reach required tolerance level" << std::endl;
          std::cout << "Average intensity difference: " << sumDiffIntensity << std::endl;
          std::cout << "-- stopping --" << std::endl;
          break;
        }
        prevSumDiffIntensity = sumDiffIntensity;
        


        //2. Compute Time Step
        //Time step
        TPrecision dt = maxMotion / ( sqrt(maxUpdate) + EPSILON );
        
        //3. Apply update time step dt
        transformUpdate->goToBegin();
        transform->goToBegin();
        maskIt.GoToBegin();
        for(;!maskIt.IsAtEnd();  transform->next(),  
            transformUpdate->next(), ++maskIt){

          if(maskIt.Get()!=0){
            transform->get(v);
            transformUpdate->get(uv);
            v += dt*uv;
            transform->set(v); 
          }
        }

        //4. Smooth vectorfield
        if(diffusionWeight > 0){
          TPrecision sigma = sqrt( 2.0 * dt * diffusionWeight );
          transform->blur(sigma);
        }

       

#ifdef VERBOSE
        std::cout << "iteration: " << nIter << std::endl;
        std::cout <<"dt: " << dt << std::endl;
        std::cout <<"MaxUpdate: " << sqrt(maxUpdate) << std::endl;
        std::cout << "average intensity difference: " << sumDiffIntensity << std::endl << std::endl;
#endif
      }

#ifdef VERBOSE
      std::cout << "nIterations: " << nIter << std::endl;
#endif

      //delete transformTmp;
      delete transformUpdate;
      return transform;
    };


    //Diffusionweight, smoothness of vectorfield
    void setDiffusionWeight(TPrecision diffw){
      diffusionWeight = diffw;
    };
    
    TPrecision getDiffusionWeight(){
      return diffusionWeight;
    };

    //motion scaling per iteration
    void setMaximumMotion(TPrecision maxm){
      maxMotion = maxm;
    };

    //mx number of iterations
    void setMaximumIterations(int maxIter){
      nIterations = maxIter;
    };

    //Stopping criterium: average pixel intensity difference < difference tolerance 
    void setDifferenceTolerance(TPrecision tol){
      diffTolerance = tol;
    }; 
    
    TPrecision getDifferenceTolerance(){
      return diffTolerance;
    };

    TPrecision getResultDifference(){
      return diffEnd;
    };


  private:

    typedef typename itk::GradientImageFilter<Image, TPrecision, TPrecision>
      GradientImageFilter;
    typedef typename GradientImageFilter::Pointer GradientImageFilterPointer;

    typedef typename GradientImageFilter::OutputImageType GradientImage;
    typedef typename GradientImage::Pointer GradientImagePointer;
    typedef typename GradientImage::PixelType GradientType;

    typedef typename itk::ImageRegionIterator<Image> ImageIterator;
    typedef typename itk::ImageRegionIteratorWithIndex<Image> IndexedImageIterator;
    typedef typename itk::ImageRegionIterator<GradientImage> GradientImageIterator;

    typedef typename itk::VectorLinearInterpolateImageFunction<GradientImage, TPrecision>
      GradientLinearInterpolate;
    typedef typename GradientLinearInterpolate::Pointer GradientLinearInterpolatePointer;
    typedef typename GradientLinearInterpolate::OutputType
      GradientInterpolateOutput;

    
    typedef typename itk::SmoothingRecursiveGaussianImageFilter<Image, Image> GaussianFilter;
    typedef typename GaussianFilter::Pointer GaussianFilterPointer;




    TPrecision maxMotion;
    TPrecision diffTolerance;
    int nIterations;
    TPrecision diffusionWeight;
    TPrecision diffEnd;


};


#endif
