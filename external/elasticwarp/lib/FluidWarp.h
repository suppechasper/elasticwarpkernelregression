#ifndef SIMPLEFLOW_H
#define SIMPLEFLOW_H

#include "VectorFieldTransform.h"

#include "ImageIO.h"

#include "itkGradientImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkMinimumMaximumImageFilter.h"
#include "itkDiscreteGaussianImageFilter.h"
#include "itkSmoothingRecursiveGaussianImageFilter.h"
#include "itkLinearInterpolateImageFunction.h"
#include "itkVectorLinearInterpolateImageFunction.h"

template<typename TImage>
class SimpleFlow{
 
  public:


    typedef TImage Image;
    typedef typename Image::Pointer ImagePointer;
    typedef typename Image::PixelType Precision;
    typedef typename Image::IndexType ImageIndex;

    typedef typename itk::LinearInterpolateImageFunction<Image, Precision>
      LinearInterpolate;
    typedef typename LinearInterpolate::Pointer LinearInterpolatePointer;
    typedef typename LinearInterpolate::ContinuousIndexType ImageContinuousIndex;

    typedef VectorFieldTransform<Image, Precision, LinearInterpolate>
      ImageTransform;

    typedef typename ImageTransform::VImage VImage;
    typedef typename VImage::VectorType VectorType;




    SimpleFlow(){
      nIterations = 200;
      diffusionWeight = 0.05;
      diffWeightDecrease = 0.001;
      diffWeightTolerance = 0.01;
      maxMotion = 0.8;
      diffTolerance = 0.01;
    };



    //Compute vectorfield v minimizing  
    // Int ( i1(x + v(x)) - i2 ) dx + alpha * Int | grad v(x) |^2 dx  
    VImage *warp(ImagePointer i1, ImagePointer i2, ImagePointer mask,
        VImage *initTransform = NULL){

      static Precision EPSILON = 1.0e-20;
      const int dimension = ::itk::GetImageDimension<Image>::ImageDimension;

      //Transformation vector field (v)
      VImage *transformPrevious; 
      if(initTransform == NULL){
        transformPrevious = ImageTransform::InitializeZeroTransform(i1); 
      }
      else{
        transformPrevious->copy(initTransform);
      }
      VImage *transformUpdate = ImageTransform::InitializeZeroTransform(i1); 
 

      //Smoothing for vectorfield
      /*transformTmp.blur(1);
      GaussianFilterPointer blur = GaussianFilter::New();
      blur->SetInput(transformTmp);
      //blur->SetSigma(1);
      blur->SetVariance(1);
      blur->Update();
      transform = blur->GetOutput();
*/

      //Interpolate function for obtaining i1(x+v(x))
      LinearInterpolatePointer i1Interpolate = LinearInterpolate::New();
      i1Interpolate->SetInputImage(i1);

      //Gradient of target image
      GradientImageFilterPointer grad = GradientImageFilter::New();
      grad->SetInput(i1);
      grad->Update(); 
      GradientImagePointer gi1 = grad->GetOutput();

      //Interpolate function for obtaining  grad(i1)(x+v(x))
      GradientLinearInterpolatePointer gi1Interpolate =
        GradientLinearInterpolate::New();
      gi1Interpolate->SetInputImage(gi1);

      //ImageIO<GradientImage>::saveImage(gi1, "grad.mhd");
      itk::Vector<Precision, dimension> tmpVector; 

      //Vector field blurring (smoothing term)
      //GaussianFilterPointer blur = GaussianFilter::New();
      //blur->SetInput(transform);


      //Region information of transformation
      ImageRegion region = i1->GetLargestPossibleRegion();
      ImageSize size = region.GetSize();
      ImageIndex index = region.GetIndex();


      //Iterators for the various images
      VectorType v(dimension);
      VectorType uv(dimension);
      VectorType vt(dimension);
      VectorType vp(dimension);

      //VectorImageIterator vIt(transform, region); 
      //VectorImageIterator uvIt(transformUpdate, region);
      //VectorImageIterator vtIt(transformTmp, region);
      //VectorImageIterator vpIt(transformPrevious, region);
      transform->initIteration(region); 
      transformUpdate->initIteration(region); 
      transformPrevious->initIteration(region); 

      GradientImageIterator gi1It(gi1, region);
      IndexedImageIterator i1It(i1, region);
      ImageIterator i2It(i2, region);
      ImageIterator maskIt(mask, region);

      
      //number of pixels in mask
      int nValid = 0;
      for(maskIt.GoToBegin(); !maskIt.IsAtEnd(); ++maskIt){
          if(maskIt.Get() != 0){
            nValid++;
          }
      };


      //Iterate until maximum number of iterations or 
      //stopping criteria is achieved   
      int nIter = 0;     
      for(;nIter<nIterations; nIter++){
        //std::cout << "Iteration: " << i << std::endl; 


        //1. Compute Vector Field update
        Precision maxIDiff = 0;
        Precision avgIDiff = 0;
        Precision diff;
        Precision maxUpdate = 0;


        Precision sumDiff = 0;
        transformUpdate->goToBegin();
        transform->goToBegin();
        for(i1It.GoToBegin(), i2It.GoToBegin(),
            gi1It.GoToBegin(), maskIt.GoToBegin(); 
            !maskIt.IsAtEnd(); ++i1It, ++i2It, ++gi1It, transform->next(),
            transformUpdate->next(), ++maskIt){

          if(maskIt.Get() != 0){

            //location cindex = x + v(x)
            ImageIndex x = i1It.GetIndex();
            transform->get(v);
            ImageContinuousIndex cindex;

            for(int i=0; i<ImageIndex::GetIndexDimension(); i++){
              cindex[i] = x[i] + v[i];
              //check range of index
              if(cindex[i] < index[i]){
                cindex[i] = index[i];
              }
              else if(cindex[i] > index[i] + size[i] - 1){
                cindex[i] = index[i] + size[i] - 1;
              }
            };

            //Gradient of i1 at x + v(x) 
            GradientInterpolateOutput g = gi1Interpolate->EvaluateAtContinuousIndex(cindex);

            //Value of i1 at x + v(x)
            Precision i1w = i1Interpolate->EvaluateAtContinuousIndex(cindex);

            tmpVector = g.GetDataPointer();
            diff = i1w -i2It.Get(); 
            sumDiff += diff*diff;
            tmpVector *= -(diff);
            Precision tmpLength = tmpVector.GetSquaredNorm();
            if(tmpLength > maxUpdate){
              maxUpdate = tmpLength;
            }
            transformUpdate->set(  tmpVector  );
          }
        }

        sumDiff = sqrt(sumDiff/nValid);
        std::cout << "average intensity difference: " << sumDiff << std::endl;

        //Check if we are within tolerance
        if(sumDiff < diffTolerance){
          break;
        }


        //2. Compute Time Step
        //std::cout <<"MaxUpdate: " << sqrt(maxUpdate) << std::endl;
        //Time step
        Precision dt = maxMotion/( sqrt(maxUpdate) + EPSILON );
        //std::cout <<"dt: " << dt << std::endl;

        
        //3. Smooth update vectorfield
        if(diffusionWeight > 0){
          Precision sigma = sqrt( 2.0 * dt * diffusionWeight );
          transformUpdate->blur(sigma);
        }

        //4. Apply update time step dt
        transformUpdate->goToBegin();
        transform->goToBegin();
        for(maskIt.GoToBegin(); maskIt.IsAtEnd(); 
            transform->next(),  transformUpdate->next(), 
             ++maskIt){

          if(maskIt.Get()!=0){
            transform->get(v);
            transformUpdate->get(uv);
            v += dt*uv;
            transform->set(v); 
          }
        }


       
        //5. Compute update difference
        transform->goToBegin();
        transformPrevious->goToBegin();

        sumDiff = 0; 
        for(maskIt.GoToBegin(); !maskIt.IsAtEnd(); transform->next(),
            transformUpdate->next(), ++maskIt){

          if(maskIt.Get()!=0){
            transform->get(v);
            transformPrevious->get(vp);
            diff = (vp - v).GetSquaredNorm();
            sumDiff += diff;
            transformPrevious->set(v);
          }
        }

        sumDiff = sqrt(sumDiff / nValid);

        //Check stopping criteria
        if(sumDiff < diffWeightTolerance ){
          if(diffWeightDecrease== 0){
            break;
          }
         diffusionWeight -= diffWeightDecrease;
         std::cout << std::endl << "DiffusionWeight: " << diffusionWeight << std::endl; 
        }
        std::cout << "Average Vector difference: " << sumDiff << std::endl;
        
  

  /*
        std::cout << "iteration: " << i << std::endl;
        std::cout << "MaxDiff field: " << maxDiff << std::endl;
        std::cout << "AvgDiff field: " << sumDiff << std::endl;

        std::cout << "MaxDiff intensity: " << maxIDiff << std::endl;
        std::cout << "AvgDiff intensity: " << avgIDiff / nValid << std::endl;
  */

       
        //std::cout << " done" << std::endl;          
      }

      std::cout << "nIterations: " << nIter << std::endl;


      delete transformPrevious;
      delete transformUpdate;
      return transform;
    };


    //Diffusionweight, smoothness of vectorfield
    void setDiffusionWeight(Precision diffw){
      diffusionWeight = diffw;
    };
    
    Precision getDiffusionWeight(){
      return diffusionWeight;
    };

    //motion scaling per iteration
    void setMaximumMotion(Precision maxm){
      maxMotion = maxm;
    };

    //mx number of iterations
    void setMaximumIterations(int maxIter){
      nIterations = maxIter;
    };

    //Stopping criterium: average pixel intensity difference < difference tolerance 
    void setDifferenceTolerance(Precision tol){
      diffTolerance = tol;
    };
    
    //Diffusion weight decrease when update transform vector field is smaller
    //than some tolerance
    void setDiffusionWeightDecreaseTolerance(Precision tol){
      diffWeightTolerance = tol;
    };

    //Tolerance for decreasing diffusion weight: average length between previous
    //and current transform vector < tolerance
    //If set to 0 warp will stop when decreaetol is reached
    void setDiffusionWeightDecrease(Precision tol){
      diffWeightDecrease = tol;
    };


    VImage *getVectorField(){
      return transform;
    };




  private:

    typedef typename itk::GradientImageFilter<Image, Precision, Precision>
      GradientImageFilter;
    typedef typename GradientImageFilter::Pointer GradientImageFilterPointer;

    typedef typename GradientImageFilter::OutputImageType GradientImage;
    typedef typename GradientImage::Pointer GradientImagePointer;
    typedef typename GradientImage::PixelType GradientType;

    typedef typename itk::ImageRegionIterator<Image> ImageIterator;
    typedef typename itk::ImageRegionIteratorWithIndex<Image> IndexedImageIterator;
    typedef typename itk::ImageRegionIterator<GradientImage> GradientImageIterator;

    typedef typename itk::VectorLinearInterpolateImageFunction<GradientImage, Precision>
      GradientLinearInterpolate;
    typedef typename GradientLinearInterpolate::Pointer GradientLinearInterpolatePointer;
    typedef typename GradientLinearInterpolate::OutputType
      GradientInterpolateOutput;

    





    VImage *transform;
    Precision maxMotion;
    
    Precision diffTolerance;
    
    int nIterations;
    
    Precision diffusionWeight;
    Precision diffWeightTolerance;
    Precision diffWeightDecrease;


};


#endif
