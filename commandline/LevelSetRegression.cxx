//Kernel Regression for image data sets
//creates kernel regressed images 


#include "MyConfig.h"

#include "IO.h"
#include "LinalgIO.h"
#include "ImageIO.h"
#include "LevelSetKernelRegression.h"
#include "GaussianKernel.h"

#include "DenseVector.h"
#include "DenseMatrix.h"


#include <tclap/CmdLine.h>



typedef Image::Pointer ImagePointer;
typedef Image::RegionType ImageRegion;
typedef ImageVectorConverter<Image> Converter;



int main(int argc, char **argv){
  using namespace FortranLinalg;

  //Command line parsing
  TCLAP::CmdLine cmd("Regress with first order level set metric", ' ', "1");

  TCLAP::ValueArg<std::string> listArg("l","list",
      "List of image to compute warps for.", 
      true, "", "filename");
  cmd.add(listArg);
  


  TCLAP::ValueArg<Precision> stepArg("s","scaling",
      "scaling of maximum step size for gradient descent", false, (Precision)0.8, 
      "step size");
  cmd.add(stepArg);    
  
  TCLAP::ValueArg<Precision> epsArg("","eps",
      "epsilon for level set metric", false, 1.f, 
      "epsilon");
  cmd.add(epsArg);  
  
  TCLAP::ValueArg<Precision> stopArg("","stop",
      "minimum amount of change per iteration before stopping", false, 1.f, 
      "step size");
  cmd.add(stopArg);

  TCLAP::ValueArg<Precision> sigmaArg("","sigma",
      "kernel sigma", true, 1, "kernel bandwidth");
  cmd.add(sigmaArg);

  TCLAP::ValueArg<int> iterArg("i","iterations",
      "maximum number of iterations per scale", false, 100, 
      "int");
  cmd.add(iterArg);

  TCLAP::ValueArg<std::string> paramArg("p","parametrzation",
      "Low dimensional parametrization of images", 
      true, "", "matrix hdr file");
  cmd.add(paramArg);

  TCLAP::ValueArg<std::string> evalArg("e","eval",
      "Low dimensional locations for doing regression at", 
      true, "", "matrix hdr file");
  cmd.add(evalArg);

  TCLAP::ValueArg<std::string> outArg("o","out",
      "output prefix", 
      true, "", "string");
  cmd.add(outArg);
  
  try{
    cmd.parse( argc, argv );
  } 
  catch (TCLAP::ArgException &e){ 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return -1;
  }
  


  //image list
  std::vector<std::string> imageFiles =
    IO<Precision>::readStringList(listArg.getValue());

  DenseVector<ImagePointer> images(imageFiles.size());
  for(unsigned int i=0; i<images.N(); i++){
   images(i) = ImageIO<Image>::readImage(imageFiles[i]);
  }
  //Labels
  DenseMatrix<Precision> parametrization =
    LinalgIO<Precision>::readMatrix(paramArg.getValue());

  //EvaluationPoints
  DenseMatrix<Precision> evals =
    LinalgIO<Precision>::readMatrix(evalArg.getValue());

  GaussianKernel<Precision> kernel(sigmaArg.getValue(), parametrization.M());
  LevelSetKernelRegression<Precision, Image> lr(images, parametrization, kernel);
  lr.setStep(stepArg.getValue());
  lr.setNIter(iterArg.getValue());
  lr.setStopping(stopArg.getValue());
  lr.setEpsilon(epsArg.getValue());

  DenseVector<Precision> eval(parametrization.M());
  for(unsigned int i=0; i < evals.N(); i++){
    std::cout << "Doing Kernel Regression" << std::endl;

    Linalg<Precision>::ExtractColumn(evals, i, eval);

    ImagePointer im = lr.evaluate(eval);
    
    std::cout << "Saving regressed image" << std::endl;
    std::stringstream imageFile;
    imageFile << outArg.getValue() << std::setfill('0') << std::setw(4) << i;
    imageFile << ".nrrd";
    ImageIO<Image>::saveImage(im, imageFile.str());
  }

  return 0;
}
