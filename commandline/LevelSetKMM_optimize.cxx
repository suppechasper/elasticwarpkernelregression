#include "Config.h"

#include "LinalgIO.h"
#include "LevelSetKMM.h"
#include "DenseMatrix.h"

#include <tclap/CmdLine.h>

//#include "EnableFloatingPointExceptions.h"

int main(int argc, char **argv){

  //Command line parsing
  TCLAP::CmdLine cmd("Kernel Map Manifolds optimization", ' ', "1");

  TCLAP::ValueArg<std::string> yArg("y","Y","image data", true, "",
      "image list file");
  cmd.add(yArg);

  TCLAP::ValueArg<std::string> zArg("z","Z","Inital low dimensional representation", 
      true, "", "matrix header file");
  cmd.add(zArg);  
  
  TCLAP::ValueArg<int> iterArg("i","iterations","Maximum number of iterations, default 200", 
      false, 200, "int");
  cmd.add(iterArg);
  
  TCLAP::ValueArg<float> scalingArg("s","step",
      "Gradietn descent step size", false, 0.8,"float");
  cmd.add(scalingArg);
   
  TCLAP::ValueArg<int> knnSigmaArg("","knnSigma",
      "Number of nearest neighbors to compute kernel sigma from", 
      false, 10,  "int");
  cmd.add(knnSigmaArg);  
  
  TCLAP::ValueArg<int> knnYArg("","knnY",
      "Number of nearest neighbors for kernel regression f - high dimensional space", 
      false, 100,  "int");
  cmd.add(knnYArg);

  TCLAP::ValueArg<int> knnXArg("","knnX",
      "Number of nearest neighbors for kernel regression g - high dimensional space", 
      false, 20,  "int");
  cmd.add(knnXArg);

  
  TCLAP::ValueArg<float> alphaArg("f","fudge",
      "Multipication factor of low dimensional kernel sigma", false, 1, 
      "float");
  cmd.add(alphaArg);  
  
  TCLAP::ValueArg<std::string> outArg("o","out",
      "output prefix for saving optimized Z (kernel regression parameters), projected data, reconstruction data and KMM file", 
      true, "", "filename");
  cmd.add(outArg);


    TCLAP::ValueArg<Precision> stepArg("","lsstep",
      "step size for level set regression", false, (Precision)0.8, 
      "step size");
  cmd.add(stepArg);    
  
  TCLAP::ValueArg<Precision> epsArg("","eps",
      "epsilon for level set metric", false, 1.f, 
      "epsilon");
  cmd.add(epsArg);  
  
  TCLAP::ValueArg<Precision> stopArg("","stop",
      "minimum amount of change per iteration before stopping for levelset regression", false, 1.f, 
      "step size");
  cmd.add(stopArg);


  TCLAP::ValueArg<int> lsriterArg("","lsri",
      "maximum number of iterations for leveset regression", false, 100, 
      "int");
  cmd.add(lsriterArg);

  try{
	  cmd.parse( argc, argv );
	} 
  catch (TCLAP::ArgException &e){ 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return -1;
  }
 

  //data    
  std::list<std::string> imageFiles =
    IO<Precision>::readStringList(yArg.getValue());

  DenseVector<ImagePointer> Y(imageFiles.size());
  for(unsigned int i=0; i<Y.N(); i++){
   Y(i) = ImageIO<Image>::readImage(imageFiles.front());
   imageFiles.pop_front();
  }
  
  DenseMatrix<Precision> Z = LinalgIO<Precision>::readMatrix(zArg.getValue());


  Precision alpha = alphaArg.getValue();

  int knnY = knnYArg.getValue();
  int knnX = knnXArg.getValue();
  int knnSigma = knnSigmaArg.getValue();
  int nIter = iterArg.getValue();
  Precision scaling = scalingArg.getValue();


  LevelSetKMM<Precision, Image> kmm(Y, Z, alpha, knnSigma, knnY, knnX);
  kmm.setStep(stepArg.getValue());
  kmm.setNIter(lsriterArg.getValue());
  kmm.setStopping(stopArg.getValue());
  kmm.setEpsilon(epsArg.getValue());


  kmm.gradDescent(nIter, scaling);
  
  std::string outprefix = outArg.getValue();
  std::stringstream ss1;
  ss1 << outprefix  << "_Xp.data";
  std::stringstream ss2;
  ss2 << outprefix  << "_Z.data";


  DenseMatrix<Precision> Xp = kmm.parametrize(Y);
  LinalgIO<Precision>::writeMatrix(ss1.str(), Xp);
 
  DenseMatrix<Precision> Zend = kmm.getZ();
  LinalgIO<Precision>::writeMatrix(ss2.str(), Zend);

  DenseVector<ImagePointer> Yp = kmm.reconstruct(Xp);
  for(unsigned int i=0; i<Yp.N(); i++){
    std::stringstream ss3;
    ss3 << outprefix << i << "_Yp.nrrd";
    ImageIO<Image>::saveImage(Yp(i), ss3.str());
  }
    
  Precision sigmaX = kmm.getSigmaX();
  
  std::stringstream ss4;
  ss4 << outprefix << ".kmminfo";

  std::ofstream info;
  info.open(ss4.str().c_str());
  info << "KMM" << std::endl;
  info << "Ydata: " << yArg.getValue() << std::endl;
  info << "Z_optimized: " << ss2.str() << ".hdr" << std::endl;
  info << "knnSigma: " << knnSigma << std::endl;
  info << "X_sigma: " << sigmaX << std::endl; 
  info << "Adaptive: " << false << std::endl;  
  info << "knnX: " << knnX << std::endl; 
  info << "knnY: " << knnY << std::endl; 
  info.close();

  Xp.deallocate();
 
  return 0;
}
