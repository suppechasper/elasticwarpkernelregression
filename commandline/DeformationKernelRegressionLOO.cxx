//Kernel Regression for image data sets
//creates kernel regressed images 

#include "Config.h"

#include "IO.h"
#include "LinalgIO.h"
#include "ImageIO.h"
#include "DeformationKernelRegression.h"

#include "DenseVector.h"
#include "DenseMatrix.h"


#include <tclap/CmdLine.h>

int main(int argc, char **argv){
  
  //Command line parsing
  TCLAP::CmdLine cmd("Compute elastic image warps for selected image pairs", ' ', "1");

  TCLAP::ValueArg<std::string> listArg("l","list",
      "Image population .", 
      true, "", "filename");
  cmd.add(listArg);
  
  TCLAP::ValueArg<std::string> paramArg("p","parametrzation",
      "Low dimensional parametrization of images", 
      true, "", "matrix hdr file");
  cmd.add(paramArg);

  TCLAP::ValueArg<std::string> evalArg("e","eval",
      "Low dimensional locations for doing regression at", 
      true, "", "matrix hdr file");
  cmd.add(evalArg);


  TCLAP::ValueArg<int> knnArg("k","knn",
      "number of neartest neighbors for regression", true, 1, "int");
  cmd.add(knnArg);

  TCLAP::ValueArg<std::string> maskArg("m","mask","mask image", false, "",
      "filename");
  cmd.add(maskArg);
  
  TCLAP::ValueArg<Precision> stepArg("s","step",
      "scaling of maximum step size for gradient descent", false, (Precision)0.2, 
      "step size");
  cmd.add(stepArg);

  TCLAP::ValueArg<int> iterArg("i","iterations",
      "maximum number of iterations per scale", false, 200, 
      "int");
  cmd.add(iterArg);
  
  
  TCLAP::ValueArg<bool> leaveoutArg("","leavouout",
      "leave nearest neighbor out", false, false, 
      "");
  cmd.add(leaveoutArg);


  TCLAP::ValueArg<Precision> diffusionArg("u","diffusion",
      "diffusion weight - smoothnes of deformation vectorfield", false, (Precision) 0.05, 
      "float");
  cmd.add(diffusionArg);

  TCLAP::ValueArg<Precision> tolArg("t","tolerance",
      "Difference between average intnesity updates of subsequent iterations, if difference falls below this value the registration is stopped", false, (Precision) 0.0001, 
      "tolerance");
  cmd.add(tolArg);

  TCLAP::ValueArg<Precision> sigmaArg("a","sigma",
      "Multiscale sigma of coarsest scale. Sigma is halfed at each subsequent scale"
      , false, (Precision) 8, "float");
  cmd.add(sigmaArg);

  TCLAP::ValueArg<int> nscaleArg("n","nscales",
      "Number of scales. 0 corresponds to no scales", false, 5, 
      "int");
  cmd.add(nscaleArg);

 try{
	  cmd.parse( argc, argv );
	} 
  catch (TCLAP::ArgException &e){ 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return -1;
  }
  


  //image list
  std::list<std::string> imageFiles =
    IO<Precision>::readStringList(listArg.getValue());

  //Mask
  ImagePointer mask = ImageIO<Image>::readImage(maskArg.getValue());
  
  Converter converter(mask);
  ImageRegion bounds = converter.getBounds();

  int D = converter.getD();


  //Labels
  DenseMatrix<Precision> parametrization =
    LinalgIO<Precision>::readMatrix(paramArg.getValue());

  //Fill data matrix
  DenseMatrix<Precision> data = ImageIO<Image>::readDataMatrix(converter, imageFiles);

  //Do Kernel regression at given locations
  DenseVector<Precision> im(D);
  
  DeformationKernelRegression<Precision, Image> reg(data, parametrization,
      knnArg.getValue(), mask, iterArg.getValue(), stepArg.getValue(),
      diffusionArg.getValue(), tolArg.getValue());

  //EvaluationPoints
  DenseMatrix<Precision> evals =
    LinalgIO<Precision>::readMatrix(evalArg.getValue());
  DenseVector<Precision> eval(parametrization.M());


  for(int i=0; i < evals.N(); i++){
    std::cout << "Doing Kernel Regression" << std::endl;

    Linalg<Precision>::ExtractColumn(evals, i, eval);

    reg.evaluate(eval, im, sigmaArg.getValue(), nscaleArg.getValue(),
        leaveoutArg.getValue());
    
    std::cout << "Saving regressed image" << std::endl;
    std::stringstream imageFile;
    imageFile << "Image_lo_" << std::setfill('0') << std::setw(4) << i;
    imageFile << "_knn_" << knnArg.getValue() << ".mhd";
    ImageIO<Image>::saveImage(im, imageFile.str(), converter);
  }

  return 0;
}
