//Kernel Regression for image data sets
//creates kernel regressed images 

#include "MyConfig.h"

#include "IO.h"
#include "LinalgIO.h"
#include "ImageIO.h"
#include "KernelRegression2.h"

#include "DenseVector.h"
#include "DenseMatrix.h"


typedef Image::Pointer ImagePointer;
typedef Image::RegionType ImageRegion;
typedef ImageVectorConverter<Image> Converter;

int main(int argc, char **argv){

  using namespace FortranLinalg;
  //Usage Information
  if(argc != 9){
    std::cout << "Usage:" << std::endl;
    std::cout << argv[0] << " imagelistFile labelFile labelDimension maskImage";
    std::cout << " sigmaKernel evaluationPointsFile nEvals infoFile" << std::endl << std::endl;
    
    std::cout << "imagelistFile = list of images to do kernel regression on" << std::endl;
    std::cout << "labelFile = binary file containing label values for each image in the";
    std::cout << " data set (N x labelDimension matrix)" << std::endl;
    std::cout << "labelDimension = dimension of image label values" << std::endl;
    std::cout << "evaluationPointsFile = binary file of label values to regress";
    std::cout << " at (nEvals x labelDimension matrix)" << std::endl;
    std::cout << "nEvals = number of regression points" << std::endl;
    std::cout << "distanceFile = file to save distances to train samples" << std::endl;
    std::cout << "infoFile = file to save regression info in" << std::endl;

    std::cout << std::endl;
    return 0;
  }

  
  char *imageListFileName = argv[1];
  char *labelFileName = argv[2];
  int d = atoi( argv[3] );
  char *maskFileName = argv[4];
  Precision sigmaKernel = atof(argv[5]);
  char *evaluationPointsFile = argv[6];  
  int nEvals = atoi(argv[7]);
  char *infoFile = argv[8];


  
  //image list
  std::vector<std::string> imageFiles = IO<Precision>::readStringList(imageListFileName);
  int N = imageFiles.size(); 
  std::cout << "Reading " << N << " images" << std::endl;


  //Mask
  ImagePointer mask = ImageIO<Image>::readImage(maskFileName);
  
  Converter converter(mask);
  ImageRegion bounds = converter.getBounds();

  int D = converter.getD();

  
  std::cout << "BoundingBox: " << bounds << std::endl;
  std::cout << "size Precision: " << sizeof(Precision) << std::endl;
  std::cout << "size of matrix: " << N << " x " << D << std::endl;

  //Labels
  DenseMatrix<Precision> labels(d, N);
  LinalgIO<Precision>::readMatrix(labelFileName, labels);

  //EvaluationPoints
  DenseMatrix<Precision> evals(d, nEvals);
  LinalgIO<Precision>::readMatrix(evaluationPointsFile, evals);

  //Fill data matrix
  DenseMatrix<Precision> data = ImageIO<Image>::readDataMatrix(converter, imageFiles);

  //Do Kernel regression at given locations
  DenseVector<Precision> im(D);
  
  KernelRegression2<Precision> reg(data, labels, sigmaKernel);
  int index = 0;
  DenseVector<Precision> eval(d);


  for(int i=0; i<nEvals; i++){


    std::cout << "Doing Kernel Regression at" << std::endl;

    for(int j=0; j < d; j++){
      eval(j) = evals(j, i);
      std::cout << eval(j) <<", "; 
    }
    std::cout << std::endl;

    reg.evaluate(eval, im);
    
    std::cout << "Saving regressed image" << std::endl;
    std::stringstream imageFile;
    imageFile << "Image_" << std::setfill('0') << std::setw(4) << i;
    imageFile << "_sigma_" << sigmaKernel << ".mhd";
    ImageIO<Image>::saveImage(im, imageFile.str(), converter);
  }

  //save info file
  std::cout << "Saving info file" << std::endl;
  std::ofstream info;
  info.open(infoFile);
  info << "Number of train samples: " << N << std::endl;
  info << "Dimension of train samples: " << D << std::endl;
  info << "Image Bounding Box " << std::endl;
  info << bounds << std::endl;
  info << "Precision " << sizeof(Precision) << std::endl;
  info << "Image Dimension " << DIMENSION << std::endl;
  info << "Number of evaluation points " << nEvals << std::endl;
  info << "Label dimension " << d << std::endl;
  info << "Mask file " << maskFileName << std::endl;
  info << "Label file " << labelFileName << std::endl;
  info << "Image list file " << imageListFileName << std::endl;
  info << "Eval file " << evaluationPointsFile << std::endl;

  info.close();
  return 0;
}
