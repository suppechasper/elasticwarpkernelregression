
#include "MyConfig.h"

#include "IO.h"
#include "ImageIO.h"
#include "LinalgIO.h"
#include "EuclideanMetric.h"
#include "KernelRegression2.h"

#include "DenseVector.h"
#include "DenseMatrix.h"

typedef Image::Pointer ImagePointer;
typedef Image::RegionType ImageRegion;
typedef ImageVectorConverter<Image> Converter;

void copyData(FortranLinalg::DenseMatrix<Precision> &from, FortranLinalg::DenseMatrix<Precision> to, int leaveOut){
  
  for(int i=0; i < leaveOut; i++){
      for(int j=0; j < from.M(); j++){
        to(j, i) = from(j,i);
      }
  }
  

  for(int i = leaveOut+1; i < from.N(); i++){
      for(int j=0; j < from.M(); j++){
        to(j, i-1) = from(j,i);
      }
  }
}

int main(int argc, char **argv){
  using namespace FortranLinalg;
  if(argc < 8){
    std::cout << "Usage:" << std::endl;
    std::cout << argv[0] << " imagelistFile maskImage dimension labelFileList";
     std::cout << " sigmaKernel projectKNN projectSigma [projectedPrefix]";
    std::cout << std::endl << std::endl;
    
    std::cout << "imageListFile = list of images to do PCA on" << std::endl;
    std::cout << "maskImage = mask, decides which pixel of each images are"<< std::endl;;
    std::cout << "dimensions = up tpnumber of dimension to do reconstructions for" << std::endl;
    std::cout << "labeFile = file containing list of labelFiles for with leaving";
    std::cout << "out image 1 - N  of image list in order" << std::endl;
    std::cout << std::endl << std::endl;

    return 0;
  }
 
  int argIndex = 1;
  char *imageListFile = argv[argIndex++];
  char *maskImageFile = argv[argIndex++];
  int dimensions = atoi(argv[argIndex++]); 
  char *labelFileList = argv[argIndex++];
  Precision sigmaKernel = atof(argv[argIndex++]); 
  int projectKNN = atoi(argv[argIndex++]);
  Precision projectSigma = atof(argv[argIndex++]);
  char *projectedPrefix = NULL;
  if(argIndex < argc){  
    projectedPrefix = argv[argIndex++];
  }

  //image list
  std::vector<std::string> imageFiles =
    IO<Precision>::readStringList(imageListFile);
  long N = imageFiles.size(); 
  std::cout << "Reading " << N << " images" << std::endl;


  //Mask
  ImagePointer mask = ImageIO<Image>::readImage(maskImageFile);
  
  Converter converter(mask);
  ImageRegion bounds = converter.getBounds();

  long D = converter.getD();

  
  //Fill data matrix
  std::cout << "BoundingBox: " << bounds << std::endl;
  
  std::cout << "size Precision: " << sizeof(Precision) << std::endl;
  std::cout << "size of matrix: " << D << " x " << N << std::endl;

  
  DenseMatrix<Precision> data = ImageIO<Image>::readDataMatrix(converter, imageFiles);
  //Labels
  std::vector<std::string> labelsList = IO<Precision>::readStringList(labelFileList);
 
  DenseMatrix<Precision> data2(D, N-1);
  
  std::vector< DenseMatrix<Precision> > labels(dimensions);
  for(int i=0; i < dimensions; i++){
    labels[i] = DenseMatrix<Precision>(i+1, N-1);
  }  

  EuclideanMetric<Precision> metric;

  DenseVector<Precision> leftOut(D);
  DenseVector<Precision> regressed(D);
  for(int i=0; i < N; i++){
    copyData(data, data2,  i);
    for(int j=0; j < D; j++){
      leftOut(j) = data(j, i);
    }

    std::string labelFile = labelsList[i];
    DenseMatrix<Precision> labelsAll = LinalgIO<Precision>::readMatrix(labelFile.c_str());

    //reconstruct for each dimension
    for(int j=1; j <= dimensions; j++){
      
      for(int m = 0; m < j; m++){
        for(int n=0; n< N-1; n++){
          labels[j-1](m, n) = labelsAll(m, n);
        }
      }

      KernelRegression2<Precision> reg(data2, labels[j-1], sigmaKernel);
      DenseVector<Precision> projected = reg.project(leftOut, projectKNN, projectSigma);
      reg.evaluate(projected, regressed); 
      projected.deallocate();  


      std::cout << metric.distance( leftOut, regressed ) << "; ";  

      if(projectedPrefix != NULL){
        std::stringstream ss;
        ss << projectedPrefix << "d_" <<std::setw(5) << std::setfill('0') << j;
        ss << "_Image_" << std::setw(5) << std::setfill('0') << i << ".mhd";
        ImageIO<Image>::saveImage(regressed, ss.str().c_str(), converter);
      }


    }
    labelsAll.deallocate();
    std::cout << std::endl;
  } 
  
  return 0;
}
