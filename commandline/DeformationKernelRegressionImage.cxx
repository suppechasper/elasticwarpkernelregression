//Kernel Regression for image data sets
//creates kernel regressed images 


#include "MyConfig.h"

#include "IO.h"
#include "LinalgIO.h"
#include "ImageIO.h"
#include "DeformationKernelRegression.h"

#include "DenseVector.h"
#include "DenseMatrix.h"


#include <tclap/CmdLine.h>
#include "ImageVectorConverter.h"


typedef Image::Pointer ImagePointer;
typedef ImageVectorConverter<Image> Converter;

int main(int argc, char **argv){
      using namespace FortranLinalg;
  
  //Command line parsing
  TCLAP::CmdLine cmd("Regress with elastic image warps", ' ', "1");

  TCLAP::ValueArg<std::string> listArg("l","list",
      "List of image to compute warps for.", 
      true, "", "filename");
  cmd.add(listArg);
  

  TCLAP::ValueArg<std::string> maskArg("m","mask","mask image", true, "",
      "filename");
  cmd.add(maskArg);

  TCLAP::ValueArg<Precision> stepArg("s","scaling",
      "scaling of maximum step size for gradient descent", false, (Precision)0.8, 
      "step size");
  cmd.add(stepArg);

  TCLAP::ValueArg<int> iterArg("i","iterations",
      "maximum number of iterations per scale", false, 200, 
      "int");
  cmd.add(iterArg);

  TCLAP::ValueArg<Precision> alphaArg("","alpha",
      "weight of gradient (alpha) and identity (1-alpha) penalty", false, (Precision) 0.5, 
      "float");
  cmd.add(alphaArg);
  

  TCLAP::ValueArg<Precision> lambdaArg("","lambda",
      "inital weight of image differnce term", true, 1, 
      "float");
  cmd.add(lambdaArg);

  TCLAP::ValueArg<Precision> lambdaIncArg("","lambdaInc",
      "increase in lambda when iamges can not be registered within epsilon for inital given lambda ", 
      true, (Precision) 0.1, 
      "float");
  cmd.add(lambdaIncArg);
    
  TCLAP::ValueArg<Precision> lambdaIncTArg("","lambdaIncThreshold",
      "Threshold in intnesity difference between two consectuive updates to update lambda", 
      true, (Precision) 0.0001, 
      "float");
  cmd.add(lambdaIncTArg);

  TCLAP::ValueArg<Precision> epsArg("","epsilon",
      "Maximal root mean squared intensity difference to register images to",
      true, (Precision) 0.01, 
      "tolerance");
  cmd.add(epsArg);

  TCLAP::ValueArg<Precision> sigmaArg("","sigma",
      "Multiscale sigma of coarsest scale. Sigma is halfed at each subsequent scale"
      , false, (Precision) 8, "float");
  cmd.add(sigmaArg);

  TCLAP::ValueArg<int> nresArg("","nres",
      "Number of resolutions for multi res", true, 5, 
      "int");
  cmd.add(nresArg);  

  TCLAP::ValueArg<int> knnArg("","knn",
      "Number of nearest neighbors", true, 5, 
      "int");
  cmd.add(knnArg);

  TCLAP::ValueArg<std::string> paramArg("p","parametrzation",
      "Low dimensional parametrization of images", 
      true, "", "matrix hdr file");
  cmd.add(paramArg);

  TCLAP::ValueArg<std::string> evalArg("e","eval",
      "Low dimensional locations for doing regression at", 
      true, "", "matrix hdr file");
  cmd.add(evalArg);

  TCLAP::ValueArg<std::string> outArg("o","out",
      "output prefix", 
      true, "", "string");
  cmd.add(outArg);
  
  TCLAP::ValueArg<bool> leaveoutArg("t","leavouout",
      "leave nearest neighbor out", false, false, 
      "");
  cmd.add(leaveoutArg);


 try{
	  cmd.parse( argc, argv );
	} 
  catch (TCLAP::ArgException &e){ 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return -1;
  }
  


  //image list
  std::vector<std::string> imageFiles =
    IO<Precision>::readStringList(listArg.getValue());

  //Mask
  ImagePointer mask = ImageIO<Image>::readImage(maskArg.getValue());
  
  Converter converter(mask);
  Image::RegionType bounds = converter.getBounds();

  //Labels
  DenseMatrix<Precision> parametrization =
    LinalgIO<Precision>::readMatrix(paramArg.getValue());

  //Fill data matrix
  DenseMatrix<Precision> data = ImageIO<Image>::readDataMatrix(converter, imageFiles);

  //Do Kernel regression at given locations
  DeformationKernelRegression<Precision, Image> reg(data, parametrization,
      knnArg.getValue(), mask, iterArg.getValue(), stepArg.getValue(),
      alphaArg.getValue(), epsArg.getValue(), lambdaArg.getValue(),
      lambdaIncArg.getValue(), lambdaIncTArg.getValue(), sigmaArg.getValue(),
      nresArg.getValue(), leaveoutArg.getValue());

  //EvaluationPoints
  DenseMatrix<Precision> evals =
    LinalgIO<Precision>::readMatrix(evalArg.getValue());
  DenseVector<Precision> eval(parametrization.M());


  for(unsigned int i=0; i < evals.N(); i++){
    std::cout << "Doing Kernel Regression" << std::endl;

    Linalg<Precision>::ExtractColumn(evals, i, eval);

    DenseVector<Precision> im = reg.evaluate(eval);
    
    std::cout << "Saving regressed image" << std::endl;
    std::stringstream imageFile;
    imageFile << outArg.getValue() << std::setfill('0') << std::setw(4) << i;
    imageFile << "_knn_" << knnArg.getValue() << ".nrrd";
    ImageIO<Image>::saveImage(im, imageFile.str(), converter);
  }

  return 0;
}
