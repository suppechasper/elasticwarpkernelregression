//Kernel Regression for image data sets
//creates kernel regressed images 

#include "MyConfig.h"

#include "IO.h"
#include "LinalgIO.h"
#include "ImageIO.h"
#include "DeformationKernelRegression.h"
#include "PCA.h"

#include "DenseVector.h"
#include "DenseMatrix.h"




typedef Image::Pointer ImagePointer;
typedef Image::RegionType ImageRegion;
typedef ImageVectorConverter<Image> Converter;




void copyData(FortranLinalg::DenseMatrix<Precision> &from, FortranLinalg::DenseMatrix<Precision> &to, int
    leaveOut, int dimensions){
  
  for(int i=0; i < leaveOut; i++){
      for(int j=0; j < dimensions; j++){
        to(j, i) = from(from.M()-dimensions+j,i);
      }
  }
  

  for(int i = leaveOut+1; i < from.N(); i++){
      for(int j=0; j < dimensions; j++){
        to(j, i-1) = from(from.M()-dimensions+j,i);
      }
  }
}



int main(int argc, char **argv){

  using namespace FortranLinalg;

  //Usage Information
  if(argc < 11){
    std::cout << "Usage:" << std::endl;
    std::cout << argv[0] << " imagelistFile evFile ewFile meanFile projFile maskImage";
    std::cout << "  dimension sigmaKernel nIterations kernelCutoff";
    std::cout << " [maxMotion = 0.8] [maxIteration = 200] [diffusionWeight =  0.05]";
    std::cout << " [diffusionWeightDecrease = 0.001]  [diffusionWeightTolerance = 0.01]";
    std::cout << " [averageIntensityTol = 0.05]";

    std::cout << std::endl << std::endl;
    
    std::cout << "imagelistFile = list of images to do kernel regression on" << std::endl;
    std::cout << "labelFile = label matrix (N x labelDimension), labels for each image" << std::endl;
    std::cout << "evaluationPointsFile = matrix of label values to regress";
    std::cout << " at (nEvals x labelDimension matrix)" << std::endl;
    std::cout << "kernelCutoff = cutoff for kernel (ignoring points with values smaller";
  
    std::cout << " than cuttoff)" << std::endl;
    

    std::cout << std::endl;
    return 0;
  }

  
  int argIndex = 1;
  char *imageListFileName = argv[argIndex++];
  char *evFile = argv[argIndex++];
  char *ewFile = argv[argIndex++];
  char *meanFile = argv[argIndex++];
  char *projFile = argv[argIndex++];
  char *maskFileName = argv[argIndex++];
  int dimension = atoi(argv[argIndex++]);
  Precision sigmaKernel = atof(argv[argIndex++]);
  int nIterations = atoi(argv[argIndex++]);
  Precision kernelCutoff = atof(argv[argIndex++]);
  
  Precision maxMotion = 0.8;
  if(argIndex < argc){
    maxMotion = atof(argv[argIndex++]); 
  }
  int maxIterations = 200;
  if(argIndex < argc){
    maxIterations = atoi(argv[argIndex++]); 
  }
  Precision diffusionWeight = 0.05;
  if(argIndex < argc){
    diffusionWeight = atof(argv[argIndex++]); 
  }
  Precision diffusionWeightDecrease = 0.001;
  if(argIndex < argc){
    diffusionWeightDecrease = atof(argv[argIndex++]); 
  }
  Precision diffusionWeightTol = 0.01;
  if(argIndex < argc){
    diffusionWeightTol = atof(argv[argIndex++]); 
  }
  Precision averageIntensityTol = 0.05;
  if(argIndex < argc){
    averageIntensityTol = atof(argv[argIndex++]); 
  }
  
  //image list
  std::vector<std::string> imageFiles = IO<Precision>::readStringList(imageListFileName);
  int N = imageFiles.size(); 
  std::cout << "Reading " << N << " images" << std::endl;


  //Mask
  ImagePointer mask = ImageIO<Image>::readImage(maskFileName);
  
  Converter converter(mask);
  ImageRegion bounds = converter.getBounds();

  int D = converter.getD();

  
  std::cout << "BoundingBox: " << bounds << std::endl;
  std::cout << "size Precision: " << sizeof(Precision) << std::endl;
  std::cout << "size of matrix: " << N << " x " << D << std::endl;

  //PCA
  DenseMatrix<Precision> ev = LinalgIO<Precision>::readMatrix(evFile);
  DenseVector<Precision> ew = LinalgIO<Precision>::readVector(ewFile);

  ImagePointer mean = ImageIO<Image>::readImage(meanFile);

  DenseVector<Precision> meanVector = converter.extractVector(mean);
  PCA<Precision> pca(ev, ew, meanVector);
 
  DenseMatrix<Precision> proj = LinalgIO<Precision>::readMatrix(projFile);
  
  //Fill data matrix
  DenseMatrix<Precision> data = ImageIO<Image>::readDataMatrix(converter, imageFiles);

  DenseMatrix<Precision> lolabels(dimension, ev.N()-1);
  DenseMatrix<Precision> lodata(data.M(), data.N()-1);
  DenseVector<Precision> lo(data.M());
  DenseVector<Precision> loproj(dimension);
  DenseVector<Precision> im(data.M());


  //leave one out deformation kernel regression
  for(int i=0; i<data.N(); i++){
   
    copyData(data, lodata, i, data.M()); 
    copyData(proj, lolabels, i, dimension); 
    Linalg<Precision>::ExtractColumn(data, i, lo);

    for(int j=0; j<dimension; j++){
      loproj(j) = proj(proj.M()-dimension+j, i);
      std::cout << loproj(j) <<", ";
    }

    std::cout << std::endl;

    DeformationKernelRegression<Precision, Image> reg(lodata, lolabels, sigmaKernel, mask,
      nIterations, maxMotion, maxIterations, diffusionWeight,
      diffusionWeightDecrease, diffusionWeightTol, averageIntensityTol, kernelCutoff, 5, true);
   
    std::cout << "PCA unproject" << std::endl; 
    pca.unproject(loproj, im, false);

    ImagePointer init = converter.createImage(im);
    ImageIO<Image>::saveImage(init, "unprojected.mhd");

    std::cout << "Kernel regression" << std::endl; 
    DenseVector<Precision> im = reg.evaluate(loproj);
    
    std::cout << "Saving regressed image" << std::endl;
    std::stringstream imageFile;
    imageFile << "Image_" << std::setfill('0') << std::setw(4) << i;
    imageFile << "_sigma_" << sigmaKernel << ".mhd";
    ImageIO<Image>::saveImage(im, imageFile.str(), converter);
  }

  //save info file
  /*
  std::cout << "Saving info file" << std::endl;
  std::ofstream info;
  info.open("info.txt");
  info << "Number of train samples: " << N << std::endl;
  info << "Dimension of train samples: " << D << std::endl;
  info << "Image Bounding Box " << std::endl;
  info << bounds << std::endl;
  info << "Precision " << sizeof(Precision) << std::endl;
  info << "Image Dimension " << DIMENSION << std::endl;
  info << "Number of evaluation points " << evals.N() << std::endl;
  info << "Label dimension " << evals.M() << std::endl;
  info << "Mask file " << maskFileName << std::endl;
  info << "Label file " << labelFileName << std::endl;
  info << "Image list file " << imageListFileName << std::endl;
  info << "Eval file " << evaluationPointsFile << std::endl;

  info.close();
  */
  return 0;
}
