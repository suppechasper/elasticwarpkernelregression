#include "MyConfig.h"

#include "IO.h"
#include "LinalgIO.h"
#include "ImageIO.h"
#include "KernelRegression.h"
#include "GaussianKernel.h"

#include "Linalg.h"
#include "DenseVector.h"
#include "DenseMatrix.h"


typedef Image::Pointer ImagePointer;
typedef Image::RegionType ImageRegion;
typedef ImageVectorConverter<Image> Converter;

int main(int argc, char **argv){
  using namespace FortranLinalg;
  if(argc != 7){
    std::cout << "Usage:" << std::endl;
    std::cout << argv[0] << " imagelistFile labelFile maskImage";
    std::cout << " sigmaKernel evaluationPointsFile output";
    std::cout << std::endl;
    return 0;
  }

  int argIndex = 1;
  char *imageListFileName = argv[argIndex++];
  char *labelFileName = argv[argIndex++];
  char *maskFileName = argv[argIndex++];
  Precision sigmaKernel = atof(argv[argIndex++]);

  char *evaluationPointsFile = argv[argIndex++];  
  char *outfile = argv[argIndex++];
 

  
 
  
  //image list
  std::vector<std::string> imageFiles = IO<Precision>::readStringList(imageListFileName);
  int N = imageFiles.size(); 
  std::cout << "Reading " << N << " images" << std::endl;


  //Mask
  ImagePointer mask = ImageIO<Image>::readImage(maskFileName);
  
  Converter converter(mask);
  ImageRegion bounds = converter.getBounds();

  int D = converter.getD();

  //Labels
  DenseMatrix<Precision> labels = LinalgIO<Precision>::readMatrix(labelFileName);
  int d = labels.M();

  //EvaluationPoints
  DenseMatrix<Precision> evals = LinalgIO<Precision>::readMatrix(evaluationPointsFile);
  
  //Fill data matrix
  std::cout << "BoundingBox: " << bounds << std::endl;
  
  std::cout << "size Precision: " << sizeof(Precision) << std::endl;
  std::cout << "size of matrix: " << N << " x " << D << std::endl;

  
  DenseMatrix<Precision> data = ImageIO<Image>::readDataMatrix(converter, imageFiles);

  //Do Kernel regression
  DenseVector<Precision> im(D);
  DenseVector<Precision> sdim(D);
  std::cout << "Doing Kernel Regression" << std::endl;
  GaussianKernel<Precision> kernel(sigmaKernel);
 
  
  KernelRegression<Precision> reg(data, labels, kernel);
  int index = 0;
  DenseVector<Precision> eval(d);
  for(int i=0; i<evals.N(); i++){
  
    Linalg<Precision>::ExtractColumn(evals, i, eval);
    std::cout << std::endl;
    reg.evaluate(eval, im, sdim);
    std::stringstream imageFile;
    imageFile << outfile << std::setfill('0') << std::setw(4) << index;
    imageFile << "_sigma_" << sigmaKernel << ".mhd";
    ImageIO<Image>::saveImage(im, imageFile.str(), converter);

    ++index;
  }

  return 0;
}
